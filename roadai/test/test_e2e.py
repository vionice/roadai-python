import unittest
import logging
import os
from typing import List, Tuple
from geojson import Polygon

from roadai.api import Api

API_USERNAME: str = os.getenv('API_USERNAME', 'roadai-api-testuser')
API_PASSWORD: str = os.getenv('API_PASSWORD',
                              'ydWcqd69KfRXnLdPkpzcjLKeSSXSvBXD')


class ApiE2ETest(unittest.TestCase):
    def test_api_login(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        logging.info("authentication: {}".format(auth))
        self.assertIsInstance(auth, dict)

        api.logout()

    def test_api_shares(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        shares = api.shares()
        self.assertTrue(len(shares) > 0)

        api.logout()

    def test_api_username(self):
        api = Api()
        api.login(API_USERNAME, API_PASSWORD)

        filters = {
            "share_id": "GBEdNHFSJrPud69gh",
            "user": 'demo'
        }

        obs = api.mobileObservations(**filters)
        self.assertTrue(len(obs) > 1)

        api.logout()

    def test_api_mobileObservations(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        polygon: Polygon = Polygon([[[28.158752918243405, 61.07231076399419],
                                     [28.159857988357544, 61.068932156730554],
                                     [28.163892030715942, 61.06894253702837],
                                     [28.162057399749756, 61.072757066109844],
                                     [28.158752918243405, 61.07231076399419]]])

        filters = {
            "share_id": "GBEdNHFSJrPud69gh",
            "geometry_polygon": polygon
        }

        obs = api.mobileObservations(**filters)
        self.assertTrue(len(obs) == 1)

        api.logout()

    def test_api_mobileObservation_by_id(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        expected_id = "E2k7x0cb9bWbDrXlb2zHixJXvqOBQOfg"

        filters = {
            "_id": expected_id
        }

        obs = api.mobileObservations(**filters)
        self.assertTrue(len(obs) == 1)
        self.assertTrue(obs[0]['_id'] == expected_id)

        api.logout()

    def test_api_mobileObservations_cursor_iterator(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        filters = {
            "share_id": "GBEdNHFSJrPud69gh",
            "cursor_next": "MjAxNy0xMi0wOVQxNTo0NTo1Ni4yNDJa",
            "limit": 10
        }

        obs = api.mobileObservations(**filters)
        self.assertEqual("AidnZHDeYt7DJxIoAILqQi4Z6kgFmpGO", obs[len(obs) - 1]["_id"])

        api.logout()

    def test_api_annotations(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        polygon: Polygon = Polygon([[[28.158752918243405, 61.07231076399419],
                                     [28.159857988357544, 61.068932156730554],
                                     [28.163892030715942, 61.06894253702837],
                                     [28.162057399749756, 61.072757066109844],
                                     [28.158752918243405, 61.07231076399419]]])

        filters = {
            "share_id": "GBEdNHFSJrPud69gh",
            "geometry_polygon": polygon
        }
        annotations = api.annotations(**filters)
        self.assertEqual(len(annotations), 5)

        api.logout()

    def test_api_annotations_classifier(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        polygon: Polygon = Polygon([[[28.158752918243405, 61.07231076399419],
                                     [28.159857988357544, 61.068932156730554],
                                     [28.163892030715942, 61.06894253702837],
                                     [28.162057399749756, 61.072757066109844],
                                     [28.158752918243405, 61.07231076399419]]])

        filters = {
            "share_id": "GBEdNHFSJrPud69gh",
            "geometry_polygon": polygon
        }
        annotations = api.annotations(**filters)

        #  API doesn't yet provide _id filtering
        annotation_id = "mQjowqpVm2lkJe3IRZ2qGEAKs8D-xzEz-1510642424074"
        annotations = [a for a in annotations if a['_id'] == annotation_id]
        self.assertTrue(len(annotations) == 1)

        pred = api.frame_classifier(annotations[0]['image_url'])
        self.assertTrue(pred is not None)

        api.logout()

    def test_api_annotations_by_id(self):
        api = Api()

        auth = api.login(API_USERNAME, API_PASSWORD)
        self.assertIsInstance(auth, dict)

        expected_id = "mQjowqpVm2lkJe3IRZ2qGEAKs8D-xzEz-1510642424074"

        filters = {
            "_id": expected_id
        }

        obs = api.annotations(**filters)
        self.assertTrue(len(obs) == 1)
        self.assertTrue(obs[0]['_id'] == expected_id)

        api.logout()


if __name__ == "__main__":
    unittest.main()
