import io
import base64
import unittest
from roadai.md30 import RecordParser, MD30Parser

TEST_DATA = base64.decodebytes(
b"""
ATAAAAACAAAAAREAMDA6MTI6RjM6MzU6MEQ6RTMCDQBWQUlTQUxBLU1PUlNFAwQATUQzMADxAAAq
iQzpbQEAAKsBACIG4ABDAH0A4AcAAAAALEFcD8ZCUKgpQVCoKUGAPR5BAgLIClE/9GgvPQAAAAAA
AAAABQAAABAAAAABAAIAUDQxNDAwMDEY7gXpbQEAAIjWkz5g0Ic+H8KNPkSLbEDwp25A+n5qQMJV
bkGmm2xBbLRhQUCOX0FnwuhBzzXFQRUzGUJSeno/by2AP6R4cD+FvbdBrd1fQdJvw0GC05xCqeg/
QlwYqkIAAKU8BAAA+kIAANhBxau6Qkb+uUIkyrpCAACAPwDEkMYAdJJG8DWARbkelT8AAAAAAICv
XUEvCMBAAAAAAJRvAPEAACqJDOltAQAAqwEAIgfgAEMAfwDgBwAAAAAsQVwPxkJQqClBUKgpQYA9
HkEDArDWUD+Vw0E9AAAAAAAAAAAFAAAAEAAAAAEAAgBQNDE0MDAwMRjuBeltAQAALl6WPqE7ij5x
4pE+RItsQPCnbkD6fmpAwlVuQaabbEFstGFBQI5fQWfC6EHPNcVBFTMZQlJ6ej9vLYA/pHhwP1Oq
xkHUAFtB4hXeQYLTnEKp6D9CXBiqQgAApTwEAAD+QgAA2EGHZbpCRLe5QsyTukIAAIA/AMSVxgAG
mkZbsZFFuR6VPwAAAAAAgK9dQS8IwEAAAAAAl2sA8QAAZ4kM6W0BAACrAQAiCOAAQwCBAOAHAAAA
ACxBXA/GQlCoKUFQqClBoEcdQQMC2JVQP1UTVj0AAAAAAAAAAAUAAAAQAAAAAQACAFA0MTQwMDAx
GO4F6W0BAADWHJc+KjOKPtc3kz5Ei2xA8KduQPp+akDCVW5BpptsQWy0YUFAjl9BZ8LoQc81xUEV
MxlCUnp6P28tgD+keHA/14S6QXGPSkG648RBgtOcQqnoP0JcGKpCAAClPAQAAAFDAADYQZUNukLG
XrlCpUG6QgAAgD8A2ozGAKKMRuopgUW5HpU/AAAAAACAr11BLwjAQAAAAABuag==
""")

class MD30Test(unittest.TestCase):
    def test_md30(self):
        with io.BytesIO(TEST_DATA) as file:
            record_iter = iter(RecordParser(file, parser=MD30Parser()))
            self.assertIsInstance(next(record_iter), RecordParser.InfoRecord)
            self.assertIsInstance(next(record_iter), RecordParser.DataRecord)
            self.assertIsInstance(next(record_iter), RecordParser.DataRecord)
            self.assertIsInstance(next(record_iter), RecordParser.DataRecord)
            with self.assertRaises(StopIteration):
                next(record_iter)

if __name__ == "__main__":
    unittest.main()
