import unittest
from roadai.download import Downloader


class DownloadTest(unittest.TestCase):
    def test_download_Downloader(self):
        downloader = Downloader()

        self.assertIsInstance(downloader, Downloader)


if __name__ == "__main__":
    unittest.main()
