import logging
import re
import struct
from .md30parser import crc16xmodem

class Convert(object):
	_RE = re.compile(b'([^,]*),([^,]*),(\x01([^\x02]*)\x02([^\x03]*)\x03)([^\x04]*)\x04[\n\r]*')
	_MD30_SEND_DATA = struct.Struct("<HHHfffffBBffffII")

	nfloat = lambda x: float(x or 'NaN')
	nint = lambda x: int(x or 0)

	_MD30_CSV_BIN_MAPPING = [
		(38, lambda x: int(x) & 0xffff),
		(None, 0),
		(None, 0),
		(10, nfloat),
		(11, nfloat),
		(12, nfloat),
		(13, nfloat),
		(14, nfloat),
		(8, nint),
		(9, nint),
		(7, nfloat),
		(4, nfloat),
		(5, nfloat),
		(6, nfloat),
		(15, nint),
		(16, nint)
	]

	def __init__(self, fileobj):
		self._fileobj = fileobj
		self._log = logging.Logger("md30 legacy")
		self._buf = b''

		try:
			timestamp, name, header, columns = self._decode_csv_line()
			self._buf += create_info_record(name=name, version=0)
			self._append_senddata(timestamp, header, columns)
		except TypeError as e:
			print("FOOOOO", e)
		except RuntimeError as e:
			self._log.warning(e)

	def read(self, size):
		while len(self._buf) < size:
			try:
				timestamp, name, header, columns = self._decode_csv_line()
				self._append_senddata(timestamp, header, columns)
			except TypeError:
				break
			except RuntimeError as e:
				self._log.warning(e)
		res = self._buf[:size]
		self._buf = self._buf[size:]
		return res

	def _decode_csv_line(self):
		line = self._fileobj.readline()
		if len(line) == 0:
			return

		match = self._RE.fullmatch(line)
		if match is None:
			raise RuntimeError("invalid line data")

		checksum = sum(match.group(3)) & 0xFFFF
		try:
			if "{:X}".format(checksum) != \
					match.group(6).decode('ASCII').upper():
				raise ValueError
		except ValueError:
			raise RuntimeError("checksum mismatch")

		try:
			timestamp = int(match.group(1))
			name = match.group(2).decode()
			header = match.group(4).split(b',')
			columns = match.group(5).split(b',')
		except ValueError:
			raise RuntimeError("invalid column data")

		return timestamp, name, header, columns

	def _append_senddata(self, timestamp, header, columns):
		params = [t(columns[i]) if i != None else t for i, t in self._MD30_CSV_BIN_MAPPING]
		msgdata = self._MD30_SEND_DATA.pack(*params)
		msg = create_md30_message(int(header[2]), 0, 0x20, 0, msgdata)
		record = create_data_record(timestamp, msg)
		self._buf += record


_MD30_HEADER = struct.Struct("<BBBBBHBB")
_MD30_FOOTER = struct.Struct("<H")
def create_md30_message(senderid, receiverid, msgid, msgnum, msgdata):
	l = len(msgdata) + 2
	if l > 65535:
		raise ValueError("msgdata too long")

	msg = _MD30_HEADER.pack(0xAB, senderid, receiverid, msgid, msgnum, l, 67, 0)
	msg += msgdata
	msg += _MD30_FOOTER.pack(crc16xmodem(msg[1:], 0xFFFF))
	return msg

def create_info_record(name=None, version=None, address=None):
	data = b''
	if version is not None:
		data += struct.pack("<BHI", 0, 4, version)
	if name is not None:
		d = name.encode()
		data += struct.pack("<BH", 1, len(d)) + d
	if address is not None:
		d = address.encode()
		data += struct.pack("<BH", 2, len(d)) + d
	return create_record(0x01, data)

_RECORD_DATA_TIMESTAMP = struct.Struct("<Q")
def create_data_record(timestamp, data):
	data = _RECORD_DATA_TIMESTAMP.pack(timestamp) + data
	return create_record(0x00, data)

_RECORD_HEADER = struct.Struct("<I")
def create_record(record_type, data):
	l = len(data)
	if l > 16777215:
		raise ValueError("data too long")

	record_type = int(record_type)
	if record_type < 0 or record_type > 255:
		raise ValueError("invalid record_type")

	record = _RECORD_HEADER.pack(l << 8 | record_type)
	record += data
	return record
