import struct
import inspect
import enum
import collections.abc

__all__ = [
	'MD30Error',
	'ParseError',
	'RecordError',
	'CrcMismatch',
	'InvalidMessageId',
	'InvalidLength',
	'InvalidData',
	'DataStatus',
	'Status',
	'Error',
	'SurfaceState',
	'ReferenceType',
	'GetUnitId',
	'GetFullProductInfo',
	'GetUnitStatus',
	'SendData',
	'SendAdditionalData',
	'SetReferences',
	'SetRoadCoefficients',
	'StopReferenceSetting',
	'CheckReferenceStatus',
	'GetParameter',
	'SetParameter',
	'RestartUnit',
	'MD30Parser',
]

class MD30Error(RuntimeError): pass
class ParseError(MD30Error): pass
class RecordError(MD30Error): pass
class CrcMismatch(RecordError): pass
class InvalidMessageId(RecordError): pass
class InvalidLength(RecordError): pass
class InvalidData(RecordError): pass

class DataStatus(enum.IntFlag):
	AMBIENT_TEMPERATURE      = 1 << 0
	RELATIVE_HUMIDITY        = 1 << 1
	DEW_POINT_TEMPERATURE    = 1 << 2
	FROST_POINT_TEMPERATURE  = 1 << 3
	ROAD_SURFACE_TEMPERATURE = 1 << 4
	ROAD_STATE               = 1 << 5
	EN15518_STATE            = 1 << 6
	GRIP                     = 1 << 7
	WATER_LAYER_THICKNESS    = 1 << 8
	ICE_LAYER_THICKNESS      = 1 << 9
	SNOW_LAYER_THICKNESS     = 1 << 10

class Status(enum.IntFlag):
	STARTINTG_UP                                                  = 1 << 0
	REFERENCE_SETTING_ONGOING                                     = 1 << 1
	LASER_TEMPERATURE_CHANGE_IN_PROGRESS                          = 1 << 2
	WINDOW_IS_CONTAMINATED                                        = 1 << 3
	WINDOW_HEATING_DYSFUNCTIONAL                                  = 1 << 4
	LOW_INPUT_VOLTAGE                                             = 1 << 5
	HIGH_INPUT_VOLTAGE                                            = 1 << 6
	HIGH_INTERNAL_TEMPERATURE                                     = 1 << 7
	TEMPERATURE_IN_FAHRENHEIT                                     = 1 << 8
	LAYER_THICKNESS_UNIT_INCH                                     = 1 << 9
	REFERENCE_SETTING_INTERRUPTED_DUE_TO_LASER_TEMPERATURE_CHANGE = 1 << 10
	REFERENCE_SETTING_INTERRUPTED_DUE_TO_HARDWARE_ERROR           = 1 << 11
	REFERENCE_SETTING_INTERRUPTED_DUE_TO_POOR_SIGNAL_QUALITY      = 1 << 12
	REFERENCE_SETTING_INTERRUPTED_BY_CLIENT                       = 1 << 13
	LAYER_THICKNESS_UNCERTAIN                                     = 1 << 14
	DISPARATE_ROAD_TYPE                                           = 1 << 15
	LAYER_THICKNESS_UNDEF_RESULT                                  = 1 << 16
	LAYER_THICKNESS_OVER_RANGE                                    = 1 << 17

class Error(enum.IntFlag):
	SURFACE_TEMPERATURE_SENSOR_ERROR  = 1 << 0
	AMBIENT_TEMPERATURE_SENSOR_ERROR  = 1 << 1
	RELATIVE_HUMIDITY_ERROR           = 1 << 2
	WINDOW_HEAVILY_CONTAMINATED       = 1 << 3
	LASER_STATUS_ERROR                = 1 << 4
	LASER_HEATING_ERROR               = 1 << 5
	EXCESSIVE_AMBIENT_LIGHT           = 1 << 6
	RECEIVER_ERROR                    = 1 << 7
	SIGNAL_LEVEL_OUT_OF_RANGE         = 1 << 8
	SIGNAL_TOO_NOISY                  = 1 << 9
	OPTICAL_MEASUREMENT_DATA_TIME_OUT = 1 << 10
	LOW_INPUT_VOLTAGE                 = 1 << 11
	HIGH_INPUT_VOLTAGE                = 1 << 12
	FLASH_FAILURE                     = 1 << 13
	INTERNAL_TEMPERATURE_TOO_HIGH     = 1 << 14
	REFERENCE_NOT_SET                 = 1 << 15
	NOT_CALIBRATED                    = 1 << 16

class SurfaceState(enum.IntEnum):
	ERROR              = 0
	DRY                = 1
	MOIST              = 2
	WET                = 3
	WET_AND_CHEMICAL   = 4
	FROST              = 5
	SNOW               = 6
	ICE                = 7
	MOIST_AND_CHEMICAL = 8
	SLUSHY             = 9
	STREAMING_WATER    = 10
	SLIPPERY           = 11
	ICE_WATCH          = 12

class ReferenceType(enum.IntEnum):
	PLATE = 0
	ROAD  = 1

class Response(collections.abc.Mapping):
	__slots__ = (
			'sender',
			'receiver',
			'msgnum',
			'version',
		)
	_SFORMAT = "<xBBxBxxBx"

	def __init__(self, data):
		fmt = ""
		conv = []
		slots = []
		for klass in reversed(inspect.getmro(type(self))):
			fmt += klass.__dict__.get('_SFORMAT', '')
			s = klass.__dict__.get('__slots__', [])
			c = klass.__dict__.get('_CONV', [None]*len(s))
			conv.extend(c)
			slots.extend(s)
		values = struct.unpack_from(fmt, data, 0)
		for s, v, c in zip(slots, values, conv):
			if c is not None:
				v = c(v)
			setattr(self, s, v)

	def __str__(self):
		return "<{} {}>".format(
				self.__class__.__name__,
				" ".join(
					"{}={}".format(a, repr(getattr(self, a, None)))
					for c in reversed(inspect.getmro(type(self)))
					for a in getattr(c, '__slots__', [])
				),
			)

	def __getitem__(self, key):
		return getattr(self, key)

	def __iter__(self):
		mro = type(self).__mro__
		slots = set().union(*[c.__slots__ for c in mro if hasattr(c, '__slots__')])
		return (i for i in slots)

	def __len__(self):
		return len(iter(self))




class GetUnitId(Response):
	__slots__ = (
			'serial_number',
		)
	_SFORMAT = "8s"
	_CONV = (
			bytes.decode,
		)

class GetFullProductInfo(Response):
	# TODO
	pass

class GetUnitStatus(Response):
	__slots__ = (
			'status',
			'error',
		)
	_SFORMAT = "II"
	_CONV = (
			Status,
			Error,
		)

class SendData(Response):
	__slots__ = (
			'analyze_count',
			'data_status_warning',
			'data_status_error',
			'ambient_temperature',
			'relative_humidity',
			'dew_point_temperature',
			'frost_point_temperature',
			'surface_temperature',
			'surface_state',
			'surface_state_EN15518',
			'grip',
			'water_layer_thickness',
			'ice_layer_thickness',
			'snow_layer_thickness',
			'status',
			'error',
		)
	_SFORMAT = "HHHfffffBBffffII"
	_CONV = (
			None,
			DataStatus,
			DataStatus,
			None,
			None,
			None,
			None,
			None,
			SurfaceState,
			SurfaceState,
			None,
			None,
			None,
			None,
			Status,
			Error,
		)

class SendAdditionalData(Response):
	# TODO
	pass

class SetReferences(Response):
	__slots__ = (
			'success',
			'status',
			'error',
		)
	_SFORMAT = "?II"
	_CONV = (
			None,
			Status,
			Error,
		)

class SetRoadCoefficients(Response):
	__slots__ = (
			'success'
		)
	_SFORMAT = "?"

class StopReferenceSetting(Response):
	pass

class CheckReferenceStatus(Response):
	__slots__ = (
			'ongoing',
			'surface_type',
			'status',
			'error',
			'latest_failure_reason',
			'complete_percent',
		)
	_SFORMAT = "?BIIIB"
	_CONV = (
			None,
			ReferenceType,
			Status,
			Error,
			Error,
			None,
		)

class GetParameter(Response):
	# TODO
	pass

class SetParameter(Response):
	# TODO
	pass

class RestartUnit(Response):
	pass


class MD30Parser(object):
	"""
	parses MD30 messages

	from recordparser import RecordParser
	from md30parser import *
	with open('serial.bin', 'rb') as f:
		parser = MD30Parser()
		for record in RecordParser(f, parser=parser.parse):
			if isinstance(record, RecordParser.InfoRecord):
				print("info", record.info)
			elif isinstance(record, RecordParser.DataRecord):
				print("data", record.time, record.data.surface_temperature, record.data.error)
	"""
	_HEADER = struct.Struct("<BxxBxHxB")
	_FOOTER = struct.Struct("<H")

	_MSG_RESPONSES = {
		0x10: GetUnitId,
		0x11: GetFullProductInfo,
		0x12: GetUnitStatus,
		0x20: SendData,
		0x21: SendAdditionalData,
		0x22: SendData,
		0x30: SetReferences,
		0x31: SetRoadCoefficients,
		0x32: StopReferenceSetting,
		0x33: CheckReferenceStatus,
		0x40: GetParameter,
		0x41: SetParameter,
		0x50: RestartUnit,
	}

	_ERRORS = {
		0: None,
		1: CrcMismatch,
		2: InvalidMessageId,
		3: InvalidLength,
		4: InvalidData,
	}

	def _infoconv_str(x):
		return x.decode()

	infomap = {
		0x01: ("address", _infoconv_str),
		0x02: ("name", _infoconv_str),
		0x03: ("type", _infoconv_str),
	}

	def __call__(self, record):
		start, msgid, length, error = MD30Parser._HEADER.unpack_from(record, 0)
		if start != 0xAB:
			raise ParseError("invalid start marker")

		if length != len(record) - MD30Parser._HEADER.size:
			raise ParseError("invalid length")

		crc, = MD30Parser._FOOTER.unpack(record[-2:])
		if crc != crc16xmodem(record[1:-2], 0xFFFF):
			raise ParseError("invalid crc")

		err = MD30Parser._ERRORS.get(error, ParseError)
		if err is not None:
			raise err()

		response = MD30Parser._MSG_RESPONSES.get(msgid, None)
		if response is None:
			raise ParseError("unknown message id")
		return response(record)


def crc16xmodem(data, crc=0x0000):
	assert type(data) is bytes, "invalid data"
	assert type(crc) is int and 0 <= crc < 65536, "invalid crc"
	for b in data:
		crc = ((crc << 8) ^ crc16xmodemTable[(crc >> 8) ^ b]) & 0xFFFF
	return crc

crc16xmodemTable = [
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
	0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
	0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
	0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
	0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
	0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
	0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
	0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
	0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
	0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
	0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
	0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
	0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
	0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
	0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
	0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
	0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
	0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
	0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
	0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
	0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
	0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
	0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0,
]



if __name__ == '__main__':
	import argparse
	import os
	import sys
	from recordparser import RecordParser

	argparser = argparse.ArgumentParser(description='read data from serial.bin')
	argparser.add_argument('file', nargs='*', default=['/dev/stdin'])
	argparser.add_argument('--skip', '-s', default=0, type=int, metavar='N', help='skip N records, (count only data record when --data is specified)')
	argparser.add_argument('--count', '-n', default=-1, type=int, metavar='N', help='extract N records, (count only data record when --data is specified)')
	args = argparser.parse_args()

	with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as stdout:
		def show_list(record):
			if args.skip > 0:
				args.skip -= 1
				return

			if isinstance(record, RecordParser.InfoRecord):
				print("info", record.info)
			elif isinstance(record, RecordParser.DataRecord):
				print("data time={}, msg={}".format(record.time, type(record.data).__name__))
				for key in (k for k in dir(record.data) if not k.startswith('_')):
					print("  {}={}".format(key, str(getattr(record.data, key))))
				print()

			args.count -= 1
			if args.count == 0:
				sys.exit()

		md30 = MD30Parser()
		for filepath in args.file:
			with open(filepath, 'rb') as f:
				for record in RecordParser(f, parser=md30):
					show_list(record)
