""" Downloader module for data """
import logging
from typing import Dict, Optional

import requests
import time
from requests.exceptions import RequestException

class Downloader:
    def __init__(self):
        self.session = None

    def connect(self, auth_data: Dict, silent=False):
        """ Create a session and set static asset server with session"""
        if not silent and self.session is not None:
            raise TypeError("self.session is defined")

        self.session = requests.Session()
        self.set_headers(auth_data)

    def set_headers(self, auth_data: Dict):
        if not self.session:
            logging.warning("Session has not been set")
            return False

        if auth_data.get('internal'):
            headers = {
                'Host': 'superuser',
                'X-User-Id': auth_data['userId']
            }
        else:
            headers = {
                'X-Auth-Token': auth_data['authToken'],
                'X-User-Id': auth_data['userId'],
                # 'X-Client-Id': client_id
            }

        self.session.headers.update(headers)

    def download_file(self, url: str) -> Optional[bytes]:
        """ Downloads file from static asset server with session"""
        if not self.session:
            logging.warning("Session has not been set")
            return None
        try:
            resp = self.session.get(url, timeout=30)
        except RequestException as err:
            logging.error("Request error: %s", str(err))
            return None

        if resp.status_code == 200:
            return resp.content
        if resp.status_code == 404:
            logging.error("File not found: (status %d) %s", resp.status_code, url)
            return None
        if resp.status_code in [500, 502]:
            # Retry in 5 seconds on 500 or 502 error
            logging.error("Error: (status %d) %s\n %s \n", resp.status_code, url, resp.text)
            logging.info("Retrying to download: %s ...", url)
            time.sleep(5)
            return self.download_file(url)

        logging.error("Unkown: %d %s\n %s \n", resp.status_code, url, resp.text)
        return None
