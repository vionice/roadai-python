import datetime
import logging
import xml.etree.ElementTree as ET
from typing import Optional


def load_gpx_data_output_geojson(gpx_data: str) -> Optional[dict]:
    """ Inputs GPX string outputs geojson like dict without dependencies """
    tree = ET.ElementTree(ET.fromstring(gpx_data))
    root = tree.getroot()

    point_list = []
    hdop_list = []
    speed_list = []
    time_list = []
    bearing_list = []
    accuracy_list = []
    fixed_time_list = []
    timefrmt = "%Y-%m-%dT%H:%M:%S.%fZ"

    for point in root[0][0]:
        try:
            lat = float(point.attrib['lat'])
            lon = float(point.attrib['lon'])
            ele = float(getattr(point[0], 'text'))
            point_list.append([lon, lat, ele])

            hdop_list.append(float(getattr(point[1], 'text')))
            speed_list.append(float(getattr(point[2], 'text')))
            ts = getattr(point[3], 'text')
            time_list.append(datetime.datetime.strptime(ts, timefrmt))
            bearing_list.append(float(getattr(point[4][0], 'text')))
            accuracy_list.append(float(getattr(point[4][1], 'text')))
            xtime = getattr(point[4][2], 'text')
            fixed_time_list.append(datetime.datetime.strptime(xtime, timefrmt))
        except AttributeError as err:
            logging.error("Exception while parsing tree %s", str(err))
            continue

    gjsl = {
        "type": "Feature",
        "properties": {
            "speeds": speed_list,
            "times": time_list,
            "hdop": hdop_list,
            "bearings": bearing_list,
            "accuracies": accuracy_list,
            "fixed_times": fixed_time_list
        },
        "geometry": {
            "type": "LineString",
            "coordinates": point_list
        }
    }

    return gjsl
