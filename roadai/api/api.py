""" Main API """
import copy
import datetime
import os
import logging
import json
import urllib
import requests
from typing import Dict, Any, Union, List, Optional, Generator
from geojson import Polygon
from requests.exceptions import RequestException
from ..download import Downloader
from .util import getHeaders, depaginate, depaginate_gen, api_post, api_patch, api_get, _api_generic
from .parse_filters import polygonToCsv, datetimeToQueryParam
from urllib.parse import urlparse

INF = int(1e16)


def parseMobileObservationsQuery(share_id: Optional[str] = None,
                                 date_from_created: Optional[datetime.datetime] = None,
                                 date_to_created: Optional[datetime.datetime] = None,
                                 date_from_recorded: Optional[datetime.datetime] = None,
                                 date_to_recorded: Optional[datetime.datetime] = None,
                                 geometry_polygon: Optional[Polygon] = None,
                                 parameter_type: Optional[str] = None,
                                 client_id: Optional[str] = None,
                                 user: Optional[str] = None,
                                 tags: Optional[List] = None,
                                 validity: Optional[List] = None,
                                 lighting_condition: Optional[List] = None,
                                 weather_condition: Optional[List] = None,
                                 surface_condition: Optional[List] = None,
                                 surface_type: Optional[List] = None,
                                 limit: Optional[int] = INF,
                                 fields: Optional[str] = None,
                                 sort: Optional[str] = None,
                                 cursor_next: Optional[str] = None):

    params: Dict[str, Union[str, int]] = {}

    if share_id is not None:
        params.update({"share_id": share_id})

    if date_from_created is not None:
        params.update({
            "date_from_created":
            datetimeToQueryParam(date_from_created)
        })

    if date_to_created is not None:
        params.update({
            "date_to_created": datetimeToQueryParam(date_to_created)
        })

    if date_from_recorded is not None:
        params.update({
            "date_from_recorded":
            datetimeToQueryParam(date_from_recorded)
        })

    if date_to_recorded is not None:
        params.update({
            "date_to_recorded":
            datetimeToQueryParam(date_to_recorded)
        })

    if geometry_polygon is not None:
        params.update({"geometry_polygon": polygonToCsv(geometry_polygon)})

    if parameter_type is not None:
        params.update({"parameter_type": parameter_type})

    if client_id is not None:
        params.update({"client_id": client_id})

    if user is not None:
        params.update({"user": user})

    if tags is not None:
        params.update({"tags": ",".join(tags)})

    if validity is not None:
        params.update({"validity": ",".join(validity)})

    if lighting_condition is not None:
        params.update({"lighting_condition": ",".join(lighting_condition)})

    if weather_condition is not None:
        params.update({"weather_condition": ",".join(weather_condition)})

    if surface_condition is not None:
        params.update({"surface_condition": ",".join(surface_condition)})

    if surface_type is not None:
        params.update({"surface_type": ",".join(surface_type)})

    if limit is not None:
        params.update({"limit": limit})

    if fields is not None:
        params.update({"fields": fields})

    if sort is not None:
        params.update({"sort": sort})

    if cursor_next is not None:
        params.update({"next": cursor_next})

    return params


class Api:
    URL = os.getenv('API_URL', 'https://api.vionice.io/api/v1/')
    NETWORK_ANALYZER_URL = os.getenv('NETWORK_ANALYZER_URL', 'https://rfn-painter-api.vionice.io/')
    STATIC_ASSETS_URL = os.getenv('STATIC_ASSETS_URL', 'https://static.vionice.io')

    def __init__(self):
        self.auth = None
        self.downloader = Downloader()

    def internal_login(self, user_id: str) -> Dict:
        """
        Login to Vionice's Services inside the cluster

        Args:
            :param user_id: user id

        Returns:
            The authentication default.
        """

        auth = {'internal': True, 'userId': user_id}

        self.auth = auth
        self.downloader.connect(auth, silent=True)

        return self.auth

    def set_auth(self, user_id, auth_token) -> Dict:
        # 2 hours token time when token is set explicitly
        self.auth = {
            'authToken': auth_token,
            'userId': user_id,
            'token_time': datetime.datetime.now() + datetime.timedelta(minutes=120)
        }

        return self.auth

    def login(self, username: str, password: str) -> Dict:
        """ Login to Vionice's Services. Return auth_data. """

        # Check if previous auth exists
        if self.auth is not None and not self.auth.get('internal'):
            logging.info("Previous authentication exists -> Logging out first")
            self.logout()

        logging.info("Authenticating against API")

        post_data = {'username': username, 'password': password}

        try:
            resp = requests.post(
                Api.URL + "login/", data=post_data, timeout=10)
        except Exception as err:
            logging.error("Login failed: %s", str(err))
            raise err

        if resp.status_code == 200:
            logging.info(resp.text)
            authentication = resp.json().get('data', None)

            if not authentication:
                raise RuntimeError("Invalid authentication")

            authentication['token_time'] = datetime.datetime.now()
            authentication['username'] = username
            authentication['password'] = password
            self.auth = authentication

            self.downloader.connect(authentication, silent=True)

        else:
            raise RuntimeError("Failed to login: %d %s", resp.status_code, resp.text)

        return self.auth

    def logout(self) -> None:
        """ Logout from Vionice's Services. """

        logging.info("Logging out!")

        if self.auth is None:
            raise RuntimeError("Can't logout when not logged in.")

        if self.auth.get('internal'):
            self.auth = None
            return

        headers = {
            'X-Auth-Token': self.auth['authToken'],
            'X-User-Id': self.auth['userId'],
        }

        try:
            resp = requests.post(
                Api.URL + "logout/", headers=headers, timeout=30)
        except Exception as err:
            logging.error("Logout failed: %s", str(err))
            raise err

        if resp.status_code == 200:
            logging.info(resp.text)
            message = resp.json().get('data', None)

            if not message:
                raise RuntimeError("Invalid logout")

            logging.info(message['message'])
            self.auth = None
        else:
            raise RuntimeError("Failed to logout: %d %s", resp.status_code, resp.text)

    def _reset_token(self, timeout=10) -> bool:
        """ Reset authentication token in long running sessions """

        if self.auth is None:
            raise RuntimeError("User is not logged in")

        if self.auth.get('internal'):
            return False

        exp_time = self.auth['token_time'] + datetime.timedelta(
            minutes=timeout)

        outdated = exp_time < datetime.datetime.now()

        if outdated:
            logging.warning("Reseting token due to timeout")

            auth = copy.deepcopy(self.auth)

            self.logout()
            self.login(auth['username'], auth['password'])

        return outdated

    def shares(self,
               client_id: Optional[str] = None,
               limit: Optional[int] = INF,
               fields: Optional[str] = None) -> Any:

        params: Dict[str, Union[str, int]] = {}

        if client_id is not None:
            params.update({"client_id": client_id})

        if limit is not None:
            params.update({"limit": limit})

        if fields is not None:
            params.update({"fields": fields})

        self._reset_token()

        return depaginate(self.auth, Api.URL + 'shares', params=params)

    def mobileObservations(self,
                           _id: Optional[str] = None,
                           share_id: Optional[str] = None,
                           date_from_created: Optional[datetime.datetime] = None,
                           date_to_created: Optional[datetime.datetime] = None,
                           date_from_recorded: Optional[datetime.datetime] = None,
                           date_to_recorded: Optional[datetime.datetime] = None,
                           geometry_polygon: Optional[Polygon] = None,
                           parameter_type: Optional[str] = None,
                           client_id: Optional[str] = None,
                           user: Optional[str] = None,
                           tags: Optional[List] = None,
                           validity: Optional[List] = None,
                           lighting_condition: Optional[List] = None,
                           weather_condition: Optional[List] = None,
                           surface_condition: Optional[List] = None,
                           surface_type: Optional[List] = None,
                           limit: Optional[int] = INF,
                           fields: Optional[str] = None,
                           sort: Optional[str] = None,
                           cursor_next: Optional[str] = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = parseMobileObservationsQuery(
            share_id=share_id,
            date_from_created=date_from_created,
            date_to_created=date_to_created,
            date_from_recorded=date_from_recorded,
            date_to_recorded=date_to_recorded,
            geometry_polygon=geometry_polygon,
            parameter_type=parameter_type,
            client_id=client_id,
            user=user,
            tags=tags,
            validity=validity,
            lighting_condition=lighting_condition,
            weather_condition=weather_condition,
            surface_condition=surface_condition,
            surface_type=surface_type,
            limit=limit,
            fields=fields,
            sort=sort,
            cursor_next=cursor_next)

        self._reset_token()

        if _id is not None:
            json, _ = api_get(self.auth, Api.URL + 'mobileObservations/' + _id, params=params)
            return [json]
        else:
            return self._mobileObservations(params=params)

    def _mobileObservations(self, params) -> List[Any]:
        return depaginate(self.auth, Api.URL + 'mobileObservations', params=params)

    def annotations(self,
                    _id: Optional[str] = None,
                    share_id: Optional[str] = None,
                    mobile_observation_id: Optional[str] = None,
                    date_from_created: Optional[datetime.datetime] = None,
                    date_to_created: Optional[datetime.datetime] = None,
                    date_from_recorded: Optional[datetime.datetime] = None,
                    date_to_recorded: Optional[datetime.datetime] = None,
                    geometry_polygon: Optional[Polygon] = None,
                    client_id: Optional[str] = None,
                    limit: Optional[int] = INF,
                    fields: Optional[str] = None,
                    cursor_next: Optional[str] = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}

        if share_id is not None:
            params.update({"share_id": share_id})

        if mobile_observation_id is not None:
            params.update({"mobile_observation_id": mobile_observation_id})

        if date_from_created is not None:
            params.update({
                "date_from_created":
                datetimeToQueryParam(date_from_created)
            })

        if date_to_created is not None:
            params.update({
                "date_to_created":
                datetimeToQueryParam(date_to_created)
            })

        if date_from_recorded is not None:
            params.update({
                "date_from_recorded":
                datetimeToQueryParam(date_from_recorded)
            })

        if date_to_recorded is not None:
            params.update({
                "date_to_recorded":
                datetimeToQueryParam(date_to_recorded)
            })

        if geometry_polygon is not None:
            params.update({"geometry_polygon": polygonToCsv(geometry_polygon)})

        if client_id is not None:
            params.update({"client_id": client_id})

        if limit is not None:
            params.update({"limit": limit})

        if fields is not None:
            params.update({"fields": fields})

        if cursor_next is not None:
            params.update({"next": cursor_next})

        self._reset_token()

        if _id is not None:
            json, _ = api_get(self.auth, Api.URL + 'annotations/' + _id, params=params)
            return [json]

        return self._annotations(params=params)

    def _annotations(self, params: Dict[str, Union[str, int]]) -> List[Any]:
        return depaginate(self.auth, Api.URL + 'annotations', params=params)

    def _generic_data_generator(
        self,
        endpoint: str,
        params: Dict,
        page_mode: bool = False
    ) -> Generator:

        self._reset_token()

        url = f"{Api.URL}{endpoint}"
        return depaginate_gen(self.auth, url, params=params, page_mode=page_mode)

    def user(self, _id: str) -> Dict:
        self._reset_token()
        json, _ = api_get(self.auth, Api.URL + 'users/' + _id)
        return json

    def client(self, _id: str) -> Dict:
        self._reset_token()
        json, _ = api_get(self.auth, Api.URL + 'clients/' + _id)
        return json

    def labels(self, client_id: str) -> Any:
        self._reset_token()
        params: Dict[str, Union[str, int]] = {'client_id': client_id, 'limit': int(1e9)}
        return depaginate(self.auth, Api.URL + 'labels/', params=params)

    def reportJobTemplates(self,
                           client_id: Optional[str] = None,
                           limit: Optional[int] = INF,
                           cursor_next: Optional[str] = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}
        if client_id is not None:
            params.update({"client_id": client_id})

        if cursor_next is not None:
            params.update({"next": cursor_next})

        self._reset_token()
        return depaginate(self.auth, Api.URL + "reportJobTemplates", params=params)

    def reportJobs(self,
                   template_id: Optional[str] = None,
                   client_id: Optional[str] = None,
                   limit: Optional[int] = INF,
                   cursor_next: Optional[str] = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}
        if template_id is not None:
            params.update({"template_id": template_id})

        if client_id is not None:
            params.update({"client_id": client_id})

        if limit is not None:
            params.update({"limit": limit})

        if cursor_next is not None:
            params.update({"next": cursor_next})

        self._reset_token()
        return depaginate(self.auth, Api.URL + "reportJobs", params=params)

    def deleteReportJob(self, _id: str) -> bool:

        self._reset_token()
        headers = getHeaders(self.auth)
        try:
            requests.delete(
                Api.URL + "reportJobs/{}".format(_id), headers=headers, timeout=30)
        except RequestException as err:
            logging.error("IOError: %s", str(err))
            return False
        return True

    def notifications(self,
                      _id: Optional[str] = None,
                      share_id: Optional[str] = None,
                      client_id: Optional[str] = None,
                      report_job_id: Optional[str] = None,
                      date_from_created: Optional[datetime.datetime] = None,
                      date_to_created: Optional[datetime.datetime] = None,
                      date_from_updated: Optional[datetime.datetime] = None,
                      date_to_updated: Optional[datetime.datetime] = None,
                      get_content: Optional[bool] = None,
                      all_users: Optional[bool] = None,
                      limit: Optional[int] = INF,
                      cursor_next: Optional[str] = None) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}

        if share_id is not None:
            params.update({"share_id": share_id})

        if client_id is not None:
            params.update({"client_id": client_id})

        if report_job_id is not None:
            params.update({"report_job_id": report_job_id})

        if date_from_created is not None:
            params.update({
                "date_from_created":
                datetimeToQueryParam(date_from_created)
            })

        if date_to_created is not None:
            params.update({
                "date_to_created":
                datetimeToQueryParam(date_to_created)
            })

        if date_from_updated is not None:
            params.update({
                "date_from_updated":
                datetimeToQueryParam(date_from_updated)
            })

        if date_to_updated is not None:
            params.update({
                "date_to_updated":
                datetimeToQueryParam(date_to_updated)
            })

        if get_content is not None:
            params.update({"get_content": str(get_content).lower()})

        if all_users is not None:
            params.update({"all_users": str(all_users).lower()})

        if limit is not None:
            params.update({"limit": limit})

        if cursor_next is not None:
            params.update({"next": cursor_next})

        self._reset_token()

        if _id is not None:
            json, _ = api_get(self.auth, Api.URL + 'notifications/' + _id, params=params)
            return [json]

        return depaginate(self.auth, Api.URL + 'notifications', params=params)

    def notificationsOperationsRecipients(self, report_job_id: str):

        params: Dict[str, Union[str, int]] = {}

        if isinstance(report_job_id, str):
            params.update({"report_job_id": report_job_id})
        else:
            raise ValueError(
                "report_job_id should be type '{}', instead it is type: '{}'".
                format(type(''), type(report_job_id)))

        return api_get(
            self.auth,
            Api.URL + 'notifications/operations/recipients',
            params=params)

    def getNetwork(self,
                   query: Dict,
                   observation_parameter: List[str],
                   geometry_id: str,
                   geometry_polygon_area: Optional[Polygon] = None,
                   export_format: Optional[str] = None,
                   geometry_split_size: int = 100) -> bytes:

        geometry_polygon_area_str = None
        if geometry_polygon_area is not None:
            geometry_polygon_area_str = polygonToCsv(geometry_polygon_area)

        query_params = parseMobileObservationsQuery(**query)

        data = {
            "mobile_observations_query": query_params,
            "observation_parameter": observation_parameter,
            "geometry_id": geometry_id,
            "geometry_polygon_area": geometry_polygon_area_str,
            "export_format": export_format,
            "geometry_split_size": geometry_split_size,
            "auth": {
                "auth_token": self.auth['authToken'],
                "user_id": self.auth['userId']
            }
        }

        self._reset_token()
        response = requests.post(
            Api.NETWORK_ANALYZER_URL + 'export/mobile_observations/network', json=data, timeout=500)

        if response.status_code != 200:
            raise RuntimeError("getNetwork failed :: status: {} content: {}".format(response.status_code, response.content.decode(encoding='UTF-8')))

        return response.content

    def getParameterTypes(
        self,
        fields: Optional[List] = None,
        limit: Optional[int] = INF
    ) -> List[Any]:

        params: Dict[str, Union[str, int]] = {}

        if fields is not None:
            params.update({"fields": ",".join(fields)})

        if limit is not None:
            params.update({"limit": limit})

        self._reset_token()
        return depaginate(self.auth, Api.URL + "parameters", params=params)

    def create_notification(self,
                            report_job_id: str,
                            title: str,
                            content: str,
                            user_ids: List[str],
                            all_users: Optional[bool] = None,
                            share_id: Optional[str] = None,
                            client_id: Optional[str] = None,
                            send_email: Optional[bool] = None,
                            email_title: Optional[str] = None,
                            email_content: Optional[str] = None) -> Any:

        data: Dict[str, Union[str, int]] = {}

        if report_job_id is not None:
            data.update({"report_job_id": report_job_id})

        if share_id is not None:
            data.update({"share_id": share_id})

        if client_id is not None:
            data.update({"client_id": client_id})

        if user_ids is not None:
            data.update({"user_ids": ','.join(user_ids)})

        if all_users is not None:
            data.update({"all_users": str(all_users).lower()})

        if title is not None:
            data.update({"title": title})

        if content is not None:
            data.update({"content": content})

        if send_email is not None:
            data.update({"send_email": str(send_email).lower()})

        if email_title is not None:
            data.update({"email_title": email_title})

        if email_content is not None:
            data.update({"email_content": email_content})

        self._reset_token()

        return api_post(self.auth, Api.URL + 'notifications', data=data)

    def update_notification(self,
                            _id: str,
                            title: Optional[str] = None,
                            content: Optional[str] = None,
                            send_email: Optional[bool] = None,
                            email_title: Optional[str] = None,
                            email_content: Optional[str] = None,
                            user_ids: Optional[List[str]] = None,
                            preserve_read_status: Optional[bool] = None) -> Any:

        data: Dict[str, Union[str, int]] = {}

        if not isinstance(_id, str):
            raise ValueError(
                "_id should be type '{}', instead it is type: '{}'".format(
                    type(''), type(_id)))

        if title is not None:
            data.update({"title": title})

        if content is not None:
            data.update({"content": content})

        if send_email is not None:
            data.update({"send_email": str(send_email).lower()})

        if email_title is not None:
            data.update({"email_title": email_title})

        if email_content is not None:
            data.update({"email_content": email_content})

        if user_ids is not None:
            data.update({"user_ids": ','.join(user_ids)})

        if preserve_read_status is not None:
            data.update({"preserve_read_status": str(preserve_read_status).lower()})

        if not data:
            raise ValueError(
                "you should provide at least one value for the update")

        self._reset_token()

        return api_patch(
            self.auth,
            '{base}notifications/{_id}'.format(base=Api.URL, _id=_id),
            data=data)

    def download_file(self, video_doc: Dict, filename: str) -> Optional[bytes]:
        if self._reset_token():
            self.downloader.set_headers(self.auth)

        if not video_doc['video_url_hd']:
            logging.warning("Video did not contain video_url_hd")
            return None

        video_url = video_doc['video_url_hd']
        video_dir = os.path.dirname(urlparse(video_url).path)
        url = f"{Api.STATIC_ASSETS_URL}{video_dir}/{filename}"

        return self.downloader.download_file(url)

    def frame_classifier(self, url, classifier='metsateho_bottleneck'):
        url = url + '&classifier=' + classifier

        self._reset_token()

        _, headers = _api_generic(
            requests.get,
            success_code=200,
            auth_data=self.auth,
            skip_output=True,
            url=url)

        if headers is None:
            return None

        pred = headers.get('x-prediction', None)

        if pred is not None:
            pred = json.loads(pred)

        return pred

    def get_video_frame(self, video_id: str, frame_time_seconds: float) -> requests.models.Response:

        params = {
            'id': video_id,
            'time': frame_time_seconds
        }

        url = "{url}{params}".format(
            url="https://img.vionice.io/?",
            params=urllib.parse.urlencode(params)
        )

        self._reset_token()
        headers = getHeaders(self.auth)
        return requests.get(url, headers=headers, stream=True)
