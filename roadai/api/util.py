import logging
import requests
import time
from requests.exceptions import RequestException
from typing import Dict, Optional, Any, Tuple, List, Union, Generator
from urllib import parse
from copy import deepcopy

API_MAX_LIMIT = 100


def getHeaders(auth_data: dict):
    if auth_data.get('internal'):
        return {'Host': 'superuser', 'X-User-Id': auth_data['userId']}
    else:
        return {
            'X-Auth-Token': auth_data['authToken'],
            'X-User-Id': auth_data['userId']
        }


def handle_params(_params: Optional[Dict]) -> Dict:

    params = deepcopy(_params)

    if params is None:
        params = {}

    if "offset" in params:
        del params['offset']

    if "limit" not in params:
        params["limit"] = 100

    return params


def depaginate(
    auth_data: Dict,
    url: str,
    params: Dict[str, Union[str, int]]
) -> List[Any]:
    params = handle_params(params)
    gen = with_cursor(url, auth_data, params, False)
    return list(gen)


def depaginate_gen(
    auth_data: Dict,
    url: str,
    params: Dict[str, Union[str, int]],
    page_mode: bool = False
) -> Generator:
    params = handle_params(params)
    return with_cursor(url, auth_data, params, page_mode)


def with_cursor(url, auth_data, params, page_mode=False):

    count = 0

    with requests.Session() as s:

        response_limit = int(params["limit"])

        while True:

            page_limit = int(response_limit - count)
            if page_limit < API_MAX_LIMIT:
                params["limit"] = page_limit

            for i in range(5):
                try:
                    entries_page, links = api_get(auth_data, url, params=params, session=s)
                except (requests.exceptions.ChunkedEncodingError, requests.exceptions.ConnectionError) as ex:
                    sleep_secs = 2 ** (i + 1)
                    logging.exception(ex)
                    logging.warning(f"Retrying in {sleep_secs} s...")
                    time.sleep(sleep_secs)
                else:
                    break
            else:
                raise RuntimeError("Request failed after retries")

            if entries_page is None:
                break

            # Some endpoints might return single value as a dict, but the code except None or list
            if isinstance(entries_page, dict):
                entries_page = [entries_page]

            if page_mode:
                yield entries_page

            for entry in entries_page:
                count += 1
                if not page_mode:
                    yield entry

            next_cursor = parse_next_cursor(links)

            if next_cursor is None:
                break
            elif count >= int(params["limit"]):
                break

            params["next"] = next_cursor


def api_get(auth_data: dict,
            url: str,
            params: Optional[Dict[str, Union[str, int]]] = None,
            session: Optional[requests.Session] = None) -> Tuple[Any, Any]:

    method: Any
    if session:
        method = session.get
    else:
        method = requests.get

    json_data, headers = _api_generic(
        method,
        success_code=200,
        auth_data=auth_data,
        url=url,
        params=params)

    links = None
    if headers:
        links = extract_header_paging_links(headers)

    return (json_data, links)


def api_post(auth_data: dict,
             url: str,
             data: Optional[Dict[str, Union[str, int]]] = None) -> Any:
    json_data, _ = _api_generic(
        requests.post,
        success_code=201,
        auth_data=auth_data,
        url=url,
        data=data)
    return json_data


def api_patch(auth_data: dict,
              url: str,
              data: Optional[Dict[str, Union[str, int]]] = None) -> Any:
    json_data, _ = _api_generic(
        requests.patch,
        success_code=204,
        auth_data=auth_data,
        url=url,
        data=data)
    return json_data


def _api_generic(method,
                 success_code: int,
                 auth_data: dict,
                 url: str,
                 params: Optional[Dict[str, Union[str, int]]] = None,
                 data: Optional[Dict[str, Union[str, int]]] = None,
                 skip_output=False) -> Tuple[Any, Any]:
    """ Authenticated request """

    if not hasattr(method, '__call__'):
        raise ValueError("method should be callable")

    if params is None:
        params = {}

    if 'limit' not in params:
        params.update({"limit": 100})

    hdrs = getHeaders(auth_data)

    start = time.time()
    try:
        rsp = method(url, params=params, data=data, headers=hdrs, timeout=60 * 5)
    except RequestException as err:
        logging.error("IOError: {} :: {}".format(str(err), url))
        raise err
    finally:
        end = time.time()
        logging.info("roadai-python :: elapsed {:0.1f}s :: endpoint {} :: limit {} :: cursor {}".format(end - start, url, params.get('limit', None), params.get('next', None)))

    if rsp.status_code == success_code:
        try:
            if skip_output or not rsp.content:
                ret = None
            else:
                ret = rsp.json()

            return (ret, rsp.headers)
        except ValueError as err:
            raise RuntimeError("Failed to parse JSON: %s %s", rsp.text, str(err))

    elif rsp.status_code == 401:
        raise RuntimeError("Unauthorized. Check your login credentials.")
    else:
        raise RuntimeError(
            "Data fetch failed.\nStatus code: {}\nMessage: {}"
            .format(rsp.status_code, rsp.json().get('message'))
        )


def extract_header_paging_links(headers) -> Optional[Dict]:
    """ Extract links from response headers """
    link_head = headers.get('Link', None)
    if not link_head:
        return None

    link_headers = link_head.replace(" ", "").split(",")
    links = {}
    for link in link_headers:
        key = link.split(";")[1].split("=")[1].replace('"', '')
        value = link.split(";")[0]
        links[key] = value
    return links


def parse_next_cursor(links) -> Union[str, None]:
    logging.info('LINKS {}'.format(links))
    if links is None or 'cursornext' not in links or len(links['cursornext']) < 3:
        return None

    url = links['cursornext'][1:-1]
    o = parse.urlparse(url)
    qp = parse.parse_qs(o.query)

    if 'next' not in qp:
        return None

    return qp['next'][0]
