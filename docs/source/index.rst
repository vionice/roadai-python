.. roadai-python documentation master file, created by
   sphinx-quickstart on Wed Oct 31 09:54:45 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to roadai-python's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
