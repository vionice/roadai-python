import argparse
import getpass
import os
import requests
from datetime import datetime

API_BASE_URL = "https://api.vionice.io/api/v1"


def roadai_login(args):
    password = getpass.getpass(f"Password for {args.username}: ")
    r = requests.post(
        f"{API_BASE_URL}/login",
        data={
            "username": args.username,
            "password": password,
        },
    )
    if r.status_code != 200:
        raise Exception(f'Got {r.status_code}: "{r.text}" from /login')

    json_resp = r.json()
    user_id = json_resp["data"]["userId"]
    auth_token = json_resp["data"]["authToken"]
    print(f"Logged in as {args.username}")

    return (user_id, auth_token)


def roadai_logout(user_id, auth_token):
    r = requests.post(
        f"{API_BASE_URL}/logout",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
    )
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code} from /logout")
    print("Logged out successfully")


def fetch_video_frame(video_id, frame_time, image_path, user_id, auth_token):
    url = f"https://img.vionice.io/?id={video_id}&time={frame_time}"

    r = requests.get(url, headers={"X-User-Id": user_id, "X-Auth-Token": auth_token})
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code} from {url}")
    with open(image_path, "wb") as f:
        f.write(r.content)


def get_video_docs(user_id, auth_token, args, layers):
    r = requests.get(
        f"{API_BASE_URL}/mobileObservations",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
        params={
            "client_id": args.client_id,
            "share_id": args.share_id,
            "date_from_recorded": args.recorded_begin,
            "date_to_recorded": args.recorded_end,
            "parameter_type": ",".join(layers),
            "sort": "recorded_at:asc",
        },
    )
    if r.status_code == 200:
        return r.json()
    elif r.status_code != 202:
        raise Exception(f"Got {r.status_code}: {r.text} from /mobileObservations")


def get_meas_list_index(video, layer_id):
    all_meas = video["location_measurements"]
    for i, meas in enumerate(all_meas):
        if meas["parameter_type"] == layer_id:
            return i
    raise Exception(f"Layer {layer_id} not found")


def parse_timestamp(timestamp):
    return datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--username", type=str, required=True, help="RoadAI username")
    # Example: abcdefgh12345678
    parser.add_argument("--client_id", type=str, required=True, help="RoadAI client ID")
    # Example: ijklmnop90123456
    parser.add_argument("--share_id", type=str, required=True, help="RoadAI share ID")
    # Example: 2023-01-20T00:00:00.000Z
    parser.add_argument(
        "--recorded_begin",
        type=str,
        required=True,
        help="Include data recorded after this timestamp (ISO 8601)",
    )
    # Example: 2023-02-20T00:00:00.000Z
    parser.add_argument(
        "--recorded_end",
        type=str,
        required=True,
        help="Include data recorded before this timestamp (ISO 8601)",
    )
    # MD30 layers are also accepted, e.g.:
    # - SURFACE_GRIP
    # - SURFACE_STATE
    # - WATER_LAYER_THICKNESS
    # - ICE_LAYER_THICKNESS
    # - SNOW_LAYER_THICKNESS
    # - SURFACE_TEMPERATURE
    # - AIR_TEMPERATURE
    # - DEW_POINT_TEMPERATURE
    # - RELATIVE_HUMIDITY
    parser.add_argument(
        "--layers",
        default=["PAVEMENT_CONDITION_CV", "POTHOLES_CV"],
        nargs="+",
        help="Parameters to include in the report",
    )
    parser.add_argument(
        "--images-path", type=str, default=None, help="Path to save images to"
    )
    args = parser.parse_args()

    user_id, auth_token = roadai_login(args)
    print(f"Getting layers: {args.layers}")

    if args.images_path is not None:
        if not os.path.exists(args.images_path):
            os.makedirs(args.images_path)

    videos = get_video_docs(user_id, auth_token, args, args.layers)
    for video in videos:
        meas_idx = [get_meas_list_index(video, l) for l in args.layers]
        video_start_time = parse_timestamp(video["recorded_at"])

        print(f"\n{video['_id']}")
        print(f"  Recorded at: {video['recorded_at']}")
        print(f"  Download link: {video['video_url_hd']}")
        print(f"  Observations:")
        for i, ts in enumerate(video["location_times"]):
            observation_time = parse_timestamp(ts)
            video_sec = (observation_time - video_start_time).total_seconds()
            print(f"    {ts} ({video_sec:.2f} s)")

            locations = video["locations"]
            coord = (
                locations["coordinates"][i]
                if locations["type"] == "LineString"
                else locations["coordinates"]
            )
            print(f"      GPS location: {coord[0], coord[1]}")
            for j, layer in enumerate(args.layers):
                print(
                    f"      {layer}: {video['location_measurements'][meas_idx[j]]['measurements'][i]}"
                )

            if args.images_path is not None and video_sec >= 0:
                image_path = os.path.join(
                    args.images_path,
                    f"{video['_id']}_{video_sec:.3f}.jpg",
                )
                fetch_video_frame(
                    video["_id"], video_sec, image_path, user_id, auth_token
                )

    roadai_logout(user_id, auth_token)
