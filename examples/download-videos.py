""" Example downloading raw data from share """
import argparse
import getpass
import os
import sys
import logging
from typing import Optional, Dict, List
import unittest

sys.path.append("/")
from roadai.api import Api
from roadai.util import io

FR_B = "%(asctime)s,%(msecs)d %(levelname)-8s"
FR_E = " [%(filename)s:%(lineno)d] %(message)s"
DATE_FRM = "%d-%m-%Y:%H:%M:%S"
logging.basicConfig(format=FR_B + FR_E, datefmt=DATE_FRM, level=logging.INFO)


def get_share_by_name(api: Api, share_name: Optional[str]) -> Optional[Dict]:
    """Get certain share"""
    share = None
    shares = api.shares(limit=1000)

    if not share_name and shares:
        logging.warning("No specific share wanted, selecting first")
        share = shares[0]
    elif shares:
        share = next((i for i in shares if i["name"] == share_name), None)

    if share is None:
        logging.error("No share %s defined", share_name)
        return None

    return share


def download_files(
    api: Api,
    videos: List,
    output_dir: str,
    filenames: List[str],
    max_videos: int = 5,
    compress: bool = False,
) -> List[str]:
    """Load files for videos"""

    logging.info("Downloading files for %d/%d videos", len(videos), max_videos)

    downloaded_files: List[str] = []
    for idx, video in enumerate(videos):
        if idx == max_videos:
            logging.warning("Max limit reached")
            break

        for filename in filenames:
            output_path = io.get_ouput_path(video, filename, output_dir)
            if not os.path.exists(output_path):  # Do not redownload
                content = api.download_file(video, filename)
                if not content:
                    continue
                if compress:
                    io.save_bz2_file(content, output_path)
                else:
                    io.save_file(content, output_path)
                downloaded_files.append(output_path)

    return downloaded_files


def download_files_from_share(
    username: str,
    password: str,
    output_dir: str,
    max_videos: int = 5,
    share_name: Optional[str] = None,
    filenames: Optional[List[str]] = None,
) -> bool:
    """Download a share to local storage using a session"""

    api = Api()
    auth_data = api.login(username, password)
    if not auth_data:
        logging.error("No auth! %s, %s", username, password)
        return False

    share = get_share_by_name(api, share_name)
    if not share:
        return False

    client_id = share.get("client_id", None)
    if client_id is None:
        logging.error("Share %s did not have client_id", str(share))
        return False

    videos = api.mobileObservations(
        limit=max_videos, client_id=client_id, share_id=share["id"]
    )

    if not videos:
        logging.error("Did not get videos for share %s", share["id"])
        return False

    if filenames is None:
        filenames = ["video_sd.mp4"]
    files = download_files(
        api, videos, output_dir, max_videos=max_videos, filenames=filenames
    )
    api.logout()

    return any(files)


def test_download_files() -> bool:
    """Test that downloading files work"""
    username: str = os.getenv("API_USERNAME", "roadai-api-testuser")
    password: str = os.getenv("API_PASSWORD", "xxxxxx")

    output_dir = "/output"
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    return download_files_from_share(username, password, output_dir, max_videos=1)


class TestDownload(unittest.TestCase):
    """Testing downloading data to local"""

    def test_download(self) -> None:
        """Test share file download"""
        self.assertTrue(test_download_files())


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--username", type=str, required=True, help="RoadAI username")
    parser.add_argument("--share", type=str, required=True, help="RoadAI share name")
    parser.add_argument(
        "--max-videos",
        type=int,
        default=5,
        help="How many videos at maximum should be downloaded (default 5)",
    )
    parser.add_argument(
        "--filenames",
        type=str,
        default=["video_sd.mp4"],
        nargs="+",
        help="Space-separated list of filenames to download (default video_sd.mp4)",
    )
    parser.add_argument(
        "--output-path",
        type=str,
        required=True,
        help="Path where the downloaded files should be written",
    )

    args = parser.parse_args()
    password = getpass.getpass()

    download_files_from_share(
        args.username,
        password,
        args.output_path,
        max_videos=args.max_videos,
        share_name=args.share,
        filenames=args.filenames,
    )
