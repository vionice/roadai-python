""" Test parsing MD30 file """
import argparse

from md30parser import read_serial_bin


def main():
    """ Run based on command line parameters """

    argparser = argparse.ArgumentParser(description='Parse MD30 datastream')

    argparser.add_argument('file', nargs='*', default=['/dev/stdin'])
    args = argparser.parse_args()
    read_serial_bin(args.file[0])


if __name__ == '__main__':
    main()
