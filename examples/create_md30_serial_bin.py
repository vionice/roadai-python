"""
Python example for serializing MD30 binary data into the serial.bin file format
accepted by Vaisala RoadAI.

The response blobs in the data records are described in the MD30 Interface
Description manual at https://www.vaisala.com/sites/default/files/documents/MD30-Interface-Description-in-English-M212201EN.pdf
"""

import datetime
from typing import BinaryIO
from enum import IntEnum
from struct import pack

class RecordType(IntEnum):
    DATA_RECORD = 0x00
    INFO_RECORD = 0x01

"""
Base class for all MD30 records. The info block is a record, and every MD30 response
is a separate record.
"""
class Record(object):
    def __init__(self, type: RecordType):
        self.type = type

    def get_header_bytes(self, data_size: int) -> bytes:
        """
        Get the header for the record.

        The record header is a 32-bit (4 byte) unsigned int.
        The 8 rightmost bits are the record type (int):
        - 0x00 for data records
        - 0x01 for info records
        The 8 leftmost bits are the length of the record data in bytes (int).
        """

        header = pack("<I", (data_size << 8) | self.type)
        return header

    def get_data_bytes(self) -> bytes:
        return b''


"""
Encoder for the info block at the beginning of the file.

Every serial.bin file always begins with an InfoRecord. It has the
following fields in the following order:

- Field index  (1 byte):  Index for the "Version" field. Value = 0x00.
- Field length (2 bytes): Length of the "Version" field. Value = 2.
- Version      (2 bytes): serial.bin file format version. Value = 0.
- Field index  (1 byte):  Index for the "Address" field. Value = 0x01.
- Field length (2 bytes): Length of the "Address" field. Value = 0..65535.
- Address      (n bytes): Bluetooth address. Example: 'CC:33:AA:44:01:02'.
- Field index  (1 byte):  Index for the "Name" field. Value = 0x02.
- Field length (2 bytes): Length of the "Name" field. Value = 0..65535.
- Name         (n bytes): Bluetooth name. Example: 'SATEL BM10'
- Field index  (1 byte):  Index for the "Device type" field. Value = 0x03.
- Field length (n bytes): Length for the "Device type" field. Value = 0..65535.
- Device type  (n bytes): Device type. Value = 'MD30'.
"""
class InfoRecord(Record):
    def __init__(self, version: int, address: str, name: str, device_type: str):
        super().__init__(type=RecordType.INFO_RECORD)
        self.version = version
        self.address = address
        self.name = name
        self.device_type = device_type

    def get_data_bytes(self) -> bytes:
        """
        Get the bytes for the data inside the info record:
        - data format version (0)
        - bluetooth address
        - bluetooth name
        - sensor type (MD30)
        """

        # version section is 2 bytes long
        version = pack("<BHH", 0x00, 2, self.version)

        address_bytes = self.address.encode()
        address = pack("<BH", 0x01, len(address_bytes)) + address_bytes

        name_bytes = self.name.encode()
        name = pack("<BH", 0x02, len(name_bytes)) + name_bytes

        device_type_bytes = self.device_type.encode()
        device_type = pack("<BH", 0x03, len(device_type_bytes)) + device_type_bytes

        return version + address + name + device_type


"""
Encoder for the data records.

Data records have the following fields in the following order:

- Timestamp (8 bytes): Observation time. Value = Unix timestamp in milliseconds, using UTC time.
- MD30 Response blob (n bytes): Full description on page 25 of the MD30 Interface Description manual.
"""
class DataRecord(Record):
    def __init__(self, ts_ms: int, response: bytes):
        self.ts_ms = ts_ms
        self.response = response
        super().__init__(type=RecordType.DATA_RECORD)

    def get_data_bytes(self) -> bytes:
        """
        Get the bytes for the data inside the data record:
        - observation timestamp
        - MD30 response
        """

        # 8 byte timestamp
        ts = pack("<Q", self.ts_ms)

        # MD30 response message (bytes) as described in Table 13 (page 25)
        # (Response message fields) in the manual.
        return ts + self.response


"""
Class for serializing info and data records into a binary file.
"""
class RecordSerializer(object):
    def __init__(self, fp: BinaryIO):
        self.fp = fp

    def add_record(self, record: Record) -> None:
        """
        Write a new record to the binary file.
        """

        data = record.get_data_bytes()
        header = record.get_header_bytes(data_size=len(data))
        self.fp.write(header)
        self.fp.write(data)


if __name__ == '__main__':
    with open('example-serial.bin', 'wb') as fp:
        s = RecordSerializer(fp)

        # The first record is an info record; the rest are data records.
        info = InfoRecord(
            version=0,
            address='CC:33:AA:44:01:02',
            name='SATEL BM10',
            device_type='MD30'
        )
        s.add_record(info)

        # We need the timestamps in milliseconds (UTC, Unix epoch)
        ts_ms = int(datetime.datetime.utcnow().timestamp() * 1000)

        # Create 10 identical MD30 responses, increasing the observation
        # timestamp by 1 second at each iteration.
        for i in range(10):
            # MD30 binary response example directly from page 26 in the MD30
            # Interface Description manual.
            resp = b'\xab\x01\x00\x20\x0e\x36\x00\x43\x00\xd7\x08\x00\x00\x00\x00\x8f\xc2\xbf\x41\x29\x5c\x45\x42\xfb\x52\x4b\x41\xfb\x52\x4b\x41\x08\xd7\x02\x42\x01\x01\x85\xeb\x51\x3f\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x53\xe8'
            data = DataRecord(
                ts_ms=ts_ms,
                response=resp
            )
            s.add_record(data)
            ts_ms += 1000

        # Now you can validate the example-serial.bin file by running it through the
        # test_md30_parsing.py script.