""" Niko's binaryparser """
import struct
import datetime


class RecordParser(object):
    """
	parses serial.bin file format
	BNF grammar from: http://bsonspec.org/spec.html

	root             ::= record_list
	record_list      ::= record | record_list
	                   | ""
	record           ::= "\x00" uint24 data_record          data_record size is uint24 bytes, 8 + (byte*)
	                   | "\x01" uint24 info_map             info_record size is uint24 bytes, info record contains key-value pairs, common to all data records up to the next info record.
	data_record      ::= timestamp (byte*)                  
	timestamp        ::= uint64                             UNIX timestamp in ms (milliseconds from 1970-01-01T00:00:00Z)
	info_map         ::= info info_map                      
	                   | ""
	info             ::= "\x00" var16_uint                  "version" file format version (currently 0x0000)
	                   | "\x01" var16_string                "address" MD30-end bluetooth address
	                   | "\x02" var16_string                "name" MD30-end bluetooth name
	var16_string     ::= uint16 (byte*)                     variable length string
	var16_uint       ::= uint16 (byte*)                     variable length unsigned integer

	There is no file header/footer so serial.bin files can be concatenated and parsed as one.
	However the an info record should always be the first record in a file.

	usage:
	from recordparser import RecordParser
	with open('serial.bin', 'rb') as f: 
		parser = MD30Parser() 
		for record in RecordParser(f, parser=parser.parse): 
			if isinstance(record, RecordParser.InfoRecord): 
				print("info", record.info) 
			elif isinstance(record, RecordParser.DataRecord): 
				print("data", record.time, record.data)
	"""
    class ParseError(RuntimeError):
        pass

    class Record(object):
        __slots__ = tuple()

    class InfoRecord(Record):
        __slots__ = ('info', )
        _HEADER = struct.Struct("<BH")

        def __init__(self, data, parser):
            I = RecordParser.InfoRecord
            m = {}
            pos = 0
            dlen = len(data)
            pinfo = getattr(parser, 'infomap', dict())
            while dlen > pos:
                rtype, rlength = I._HEADER.unpack_from(data, pos)
                pos += I._HEADER.size
                k, f = I._MAP.get(rtype, None) or pinfo.get(
                    rtype,
                    None) or ("unknown_0x{:02X}".format(rtype), I._conv_bytes)
                d = data[pos:pos + rlength]
                pos += rlength
                if len(d) != rlength:
                    raise RecordParser.ParseError("invalid info record")
                m[k] = f(d)
            self.info = m

        def _conv_bytes(data):
            return data

        def _conv_int(data):
            v = 0
            for i in reversed(data):
                v = (v << 8) | i
            return v

        _MAP = {
            0x00: ("version", _conv_int),
        }

    class DataRecord(Record):
        __slots__ = (
            'time',
            'data',
        )
        _TIMESTAMP = struct.Struct("<Q")

        def __init__(self, data, parser):
            TS = RecordParser.DataRecord._TIMESTAMP
            ts_ms, = TS.unpack_from(data, 0)
            self.time = datetime.datetime.utcfromtimestamp(ts_ms / 1000)
            self.data = parser(data[TS.size:])

    _RECORD_HEADER = struct.Struct("<I")
    _RECORD_PARSERS = {
        0x00: DataRecord,
        0x01: InfoRecord,
    }

    def __init__(self, fileobj, parser=lambda x: x):
        self._fileobj = fileobj
        self._parser = parser

    def __iter__(self):
        return self

    def __next__(self):
        try:
            d = self._fileobj.read(RecordParser._RECORD_HEADER.size)
            if len(d) == 0:
                raise StopIteration()

            l, = RecordParser._RECORD_HEADER.unpack(d)
            t = l & 0xff
            l = l >> 8

            data = self._fileobj.read(l)
            if len(data) != l:
                raise struct.error()

            record = RecordParser._RECORD_PARSERS.get(t)
            if record is None:
                raise RecordParser.ParseError("invalid record type", t)
            return record(data, self._parser)

        except struct.error as e:
            raise RecordParser.ParseError("unexpected end of stream", e)


def main():
    import argparse
    import os
    import sys

    argparser = argparse.ArgumentParser(
        description='read data from serial.bin')
    argparser.add_argument('file', nargs='*', default=['/dev/stdin'])
    argparser.add_argument(
        '--skip',
        '-s',
        default=0,
        type=int,
        metavar='N',
        help='skip N records, (count only data record when --data is specified)'
    )
    argparser.add_argument(
        '--count',
        '-n',
        default=-1,
        type=int,
        metavar='N',
        help=
        'extract N records, (count only data record when --data is specified)')
    argparser.add_argument('--data',
                           '-d',
                           action='store_true',
                           help='extract raw data records only')
    args = argparser.parse_args()

    with os.fdopen(sys.stdout.fileno(), "wb", closefd=False) as stdout:

        def show_list(record):
            if args.skip > 0:
                args.skip -= 1
                return

            if isinstance(record, RecordParser.InfoRecord):
                print("info", record.info)
            elif isinstance(record, RecordParser.DataRecord):
                print("data time={}, size={}".format(record.time,
                                                     len(record.data)))

            args.count -= 1
            if args.count == 0:
                sys.exit()

        def extract_data(record):
            if isinstance(record, RecordParser.DataRecord):
                if args.skip > 0:
                    args.skip -= 1
                    return

                stdout.write(record.data)

                args.count -= 1
                if args.count == 0:
                    sys.exit()

        for filepath in args.file:
            with open(filepath, 'rb') as f:
                for record in RecordParser(f):
                    if args.data:
                        extract_data(record)
                    else:
                        show_list(record)


if __name__ == '__main__':
    main()
