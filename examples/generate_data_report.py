"""
Generate RoadAI data report in Excel format at 10 meter resolution.
"""

import argparse
import pandas as pd
import requests
import time

API_BASE_URL = "https://api.vionice.io/api/v1"


def roadai_login(args):
    r = requests.post(
        f"{API_BASE_URL}/login",
        data={
            "username": args.username,
            "password": args.password,
        },
    )
    if r.status_code != 200:
        raise Exception(f'Got {r.status_code}: "{r.text}" from /login')

    json_resp = r.json()
    user_id = json_resp["data"]["userId"]
    auth_token = json_resp["data"]["authToken"]
    print(f"Logged in as {args.username}")

    return (user_id, auth_token)


def roadai_logout(user_id, auth_token):
    r = requests.post(
        f"{API_BASE_URL}/logout",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
    )
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code} from /logout")
    print("Logged out successfully")


def generate_report(user_id, auth_token, args):
    payload = {
        "basic_parameters": {
            "client_id": args.client_id,
            "share_id": args.share_id,
            "user_id": user_id,
            "title": args.title,
            "filename": args.filename,
            "locale": "en-GB",
            "query": {
                "client_id": args.client_id,
                "share_id": args.share_id,
                "date_from_recorded": args.created_begin,
                "date_to_recorded": args.created_end,
                "deleted_at": args.created_end,
            },
        },
        "distance_config": {
            "dist_unit": "meter",
            "interval": 10,
            "report_whole_roads": False,
        },
        "file_format": "excel",
        "parameters": args.parameters,
        "aggregation": {
            "aggregation_defect": "select_most_severe_value",
            "aggregation_score": "select_most_severe_value",
        },
        "additional_options": {
            "directional": False,
            "multiple_drives": False,
            "merge_driving_dirs": False,
        },
        "send_email": False,
        "filters": {
            "validity": ["valid"],
            "lighting_condition": ["bright"],
            "surface_type": ["asphalt"],
            "weather_condition": ["dry"],
            "surface_condition": ["dry"],
        },
    }

    print(f"Requesting report {args.title}")

    response = requests.post(
        f"{API_BASE_URL}/mobileObservations/exports/report_road_section",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
        json=payload,
        timeout=10,
    )

    if response.status_code != 201:
        raise Exception(
            f"Got {response.status_code}: {response.text} from /mobileObservations/exports/report_road_section"
        )

    # Return the report ID
    json_resp = response.json()
    return json_resp


def poll_for_report_link(user_id, auth_token, report_id):
    print("Polling for the report download link...")

    max_attempts = 10
    for i in range(max_attempts):
        print(f"  Attempt {i+1}...")
        r = requests.get(
            f"{API_BASE_URL}/mobileObservations/exports/status/{report_id}",
            headers={
                "X-User-Id": user_id,
                "X-Auth-Token": auth_token,
            },
        )
        if r.status_code == 200:
            return r.json()
        elif r.status_code != 202:
            raise Exception(
                f"Got {r.status_code}: {r.text} from /mobileObservations/exports/status"
            )
        else:
            time.sleep(30)

    raise Exception("Polling for the export timed out")


def get_report_dataframe(report_url, filename):
    r = requests.get(report_url)
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code}: {r.text} from report URL {report_url}")

    with open(filename, "wb") as fp:
        fp.write(r.content)

    # Convert the Excel report to a Pandas DataFrame
    return pd.read_excel(filename)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--username", type=str, required=True, help="RoadAI username")
    parser.add_argument("--password", type=str, required=True, help="RoadAI password")
    # Example: abcdefgh12345678
    parser.add_argument("--client_id", type=str, required=True, help="RoadAI client ID")
    # Example: ijklmnop90123456
    parser.add_argument("--share_id", type=str, required=True, help="RoadAI share ID")
    # Example: 2023-01-20T00:00:00.000Z
    parser.add_argument(
        "--created_begin",
        type=str,
        required=True,
        help="Include data created after this timestamp (ISO 8601)",
    )
    # Example: 2023-02-20T00:00:00.000Z
    parser.add_argument(
        "--created_end",
        type=str,
        required=True,
        help="Include data created before this timestamp (ISO 8601)",
    )
    parser.add_argument("--title", type=str, default="Test report", help="Report title")
    parser.add_argument(
        "--filename", type=str, default="10m_test_report", help="Report filename"
    )
    parser.add_argument(
        "--parameters",
        choices=["PAVEMENT_CONDITION_CV", "POTHOLES_CV"],
        nargs="+",
        help="Parameters to include in the report",
    )
    args = parser.parse_args()

    user_id, auth_token = roadai_login(args)

    report_id = generate_report(user_id, auth_token, args)
    report_url = poll_for_report_link(user_id, auth_token, report_id)
    print(f"The report is ready, downloading report {report_id}...")
    df = get_report_dataframe(report_url, f"{args.filename}.xlsx")

    # Do something with the report content...
    print(df)

    roadai_logout(user_id, auth_token)
