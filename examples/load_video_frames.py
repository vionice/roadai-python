import sys
import shutil
import getpass

sys.path.append('/')
from roadai.api import Api


if __name__ == '__main__':

    # Set video_id as the id of a video you have access to
    video_id = ''
    assert len(video_id), "Please set video_id"

    api = Api()
    username = input("username: ")
    password = getpass.getpass("password: ")
    api.login(username, password)

    for frame_time_seconds in [0, 10.5, 15.7]:
        response = api.get_video_frame(video_id, frame_time_seconds)
        with open(f'img_{frame_time_seconds}.jpeg', 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)

    api.logout()
