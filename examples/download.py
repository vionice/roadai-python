""" Example downloading raw data from share """
import os
import sys
import logging
from typing import Optional, Dict, List
import unittest

sys.path.append('/')
from roadai.api import Api
from roadai.util import io

FR_B = "%(asctime)s,%(msecs)d %(levelname)-8s"
FR_E = ' [%(filename)s:%(lineno)d] %(message)s'
DATE_FRM = '%d-%m-%Y:%H:%M:%S'
logging.basicConfig(format=FR_B + FR_E, datefmt=DATE_FRM, level=logging.INFO)


def get_share_by_name(api: Api, share_name: str) -> Optional[Dict]:
    """ Get certain share """
    share = None
    shares = api.shares(limit=1000)
    if not share_name and shares:
        logging.warning("No specific share wanted, selecting first")
        share = shares[0]
    elif shares:
        share = next((i for i in shares if i['name'] == share_name), None)

    if share is None:
        logging.error("No share %s defined", share_name)
        return None
    return share


def download_files(api: Api,
                   videos: List,
                   output_dir: str,
                   download_serial=False,
                   max_videos: int = 5,
                   compress=False) -> List[str]:
    """ Load files for videos """
    download_filenames = ["location.gpx", "accelerometer.csv", "gyroscope.csv"]
    if download_serial:
        download_filenames.append("serial.csv")
        download_filenames.append("serial.bin")

    logging.info("Downloading files for %d/%d videos", len(videos), max_videos)
    downloaded_files: List[str] = []
    for idx, video in enumerate(videos):
        if idx == max_videos:
            logging.warning("Max limit reached")
            break
        for filename in download_filenames:
            output_path = io.get_ouput_path(video, filename, output_dir)
            if not os.path.exists(output_path):  # Do not redownload
                content = api.download_file(video, filename)
                if not content:
                    continue
                if compress:
                    io.save_bz2_file(content, output_path)
                else:
                    io.save_file(content, output_path)
                downloaded_files.append(output_path)
    return downloaded_files


def download_files_from_share(username: str,
                              password: str,
                              output_dir: str,
                              max_videos: int = 5,
                              share_name=None,
                              download_serial=False) -> bool:
    """ Download a share to local storage using a session """
    api = Api()
    auth_data = api.login(username, password)
    if not auth_data:
        logging.error("No auth! %s, %s", username, password)
        return False

    share = get_share_by_name(api, share_name)
    if not share:
        return False
    client_id = share.get('client_id', None)
    if client_id is None:
        logging.error("Share %s did not have client_id", str(share))
        return False

    parameter_type = None
    if download_serial is True:
        parameter_type = "SURFACE_TEMPERATURE"

    videos = api.mobileObservations(limit=max_videos,
                                    share_id=share["id"],
                                    parameter_type=parameter_type)
    if not videos:
        logging.error("Did not get videos for share %s", share['id'])
        return False

    files = download_files(api, videos, output_dir, download_serial,
                           max_videos)
    api.logout()

    if any(files):
        return True
    return False


def test_download_files() -> bool:
    """ Test that downloading files work """
    username: str = os.getenv('API_USERNAME', 'roadai-api-testuser')
    password: str = os.getenv('API_PASSWORD', 'xxxxxx')

    output_dir = "/output"
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    return download_files_from_share(username,
                                     password,
                                     output_dir,
                                     max_videos=10,
                                     download_serial=True)


class TestDownload(unittest.TestCase):
    """ Testing downloading data to local """
    def test_download(self):
        """ Test share file download """
        self.assertTrue(test_download_files())


if __name__ == "__main__":
    unittest.main()
