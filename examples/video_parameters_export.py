import os
import getpass
import pandas as pd
import numpy as np
import sys; sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from roadai.api import Api
from typing import List, Dict
from dateutil.parser import isoparse


INFINITY = int(1e9)


# Surface State - MD30 Sensor
PARAMETER_TYPES = {
    "SURFACE_STATE",
    "SURFACE_GRIP",
    "AIR_TEMPERATURE",
    "SURFACE_TEMPERATURE",
    "RELATIVE_HUMIDITY",
    "DEW_POINT_TEMPERATURE",
    "WATER_LAYER_THICKNESS",
    "SNOW_LAYER_THICKNESS",
    "ICE_LAYER_THICKNESS"
}

# Road Weather - Computer Vision
# PARAMETER_TYPES = {
#     "SURFACE_STATE_CV",
#     "SURFACE_SNOW_CV",
#     "SURFACE_WATER_CV"
# }

# Pavement Condition - Computer Vision
# PARAMETER_TYPES = {
#     "PAVEMENT_CONDITION_CV",
#     "SEVERE_POTHOLE_CV",
#     "SEVERE_CRACKING_LONGITUDINAL_CV",
# }

# Assets - Computer Vision
# PARAMETER_TYPES = {
#     "GUARD_RAIL_RIGHT_CV",
#     "GUARD_RAIL_LEFT_CV",
#     "TRAFFIC_CONE_RIGHT_CV",
#     "TRAFFIC_CONE_LEFT_CV"
# }


def get_api() -> Api:
    api = Api()
    username = input("username: ")
    password = getpass.getpass("password: ")
    api.login(username, password)
    print("Login successful!")
    return api


def get_shares(api: Api) -> List[Dict]:
    print("Loading shares...")
    shares = api.shares(limit=INFINITY)
    return shares


def select_share(shares: List[Dict]) -> Dict:

    print("Available shares:")
    for i, share in enumerate(shares):
        print(i, share['name'])

    index = int(input("share index: "))
    if not (0 <= index <= len(shares) - 1):
        raise RuntimeError("Invalid share index")

    return shares[index]


base_parameters_config = {
    'timestamp': lambda doc: [isoparse(dt_str) for dt_str in doc['location_times']],
    'longitude': lambda doc: [x[0] if x is not None else None for x in doc['locations']['coordinates']],
    'latitude': lambda doc: [x[1] if x is not None else None for x in doc['locations']['coordinates']]
}


def video_is_valid(doc: Dict) -> bool:

    if doc.get('locations') is None:
        return False

    if doc['locations']['type'] != 'LineString':
        return False

    return True


def write_doc_to_file(doc: Dict, path: str, index_map: Dict[str, int]) -> None:

    if not video_is_valid(doc):
        return

    video_id = doc['_id']

    n_base_params = len(base_parameters_config)
    n_data_points = len(doc['location_times'])
    n_params = len(PARAMETER_TYPES) + n_base_params

    headers = list(base_parameters_config.keys()) + list(PARAMETER_TYPES)
    measurements = [[None] * n_data_points] * n_params

    for i, (_, func) in enumerate(base_parameters_config.items()):
        measurements[i] = func(doc)

    for lm in doc['location_measurements']:
        param_type = lm['parameter_type']
        if param_type not in PARAMETER_TYPES:
            continue
        index = index_map[param_type] + n_base_params
        measurements[index] = lm['measurements'][:n_data_points]

    df = pd.DataFrame(np.array(measurements).T, columns=headers)

    filename = f'{video_id}.csv'
    filepath = os.path.join(path, filename)
    print(f"Saving video {video_id} to {filepath}")
    df.to_csv(filepath, index=False)


def main() -> None:

    index_map = {key: i for i, key in enumerate(PARAMETER_TYPES)}

    api = get_api()
    shares = get_shares(api)
    share = select_share(shares)

    video_limit = 10

    print(f"You have selected share: {share['name']}")
    print(f"Amount of videos limited to: {video_limit}")

    video_query = {
        'client_id': share['client_id'],
        'share_id': share['id'],
        'limit': video_limit
    }

    print("Fetching mobile observation documents from API...")
    obs = api.mobileObservations(**video_query)

    path = os.path.join(
        'video_parameter_files',
        share['client_id'],
        share['id']
    )

    if not os.path.isdir(path):
        os.makedirs(path)

    for doc in obs:
        write_doc_to_file(doc, path, index_map)

    api.logout()


if __name__ == '__main__':
    main()
