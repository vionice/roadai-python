import argparse
import getpass
import requests
from typing import Optional, Tuple

API_BASE_URL = "https://api.vionice.io/api/v1"


def roadai_login(
    args: argparse.Namespace,
    password: Optional[str] = None,
    mfa_code: Optional[str] = None,
) -> Tuple[str, str]:
    if password is None:
        password = getpass.getpass("Password: ")
    data = {
        "username": args.username,
        "password": password,
    }
    if mfa_code is not None:
        data["mfa_code"] = mfa_code

    r = requests.post(
        f"{API_BASE_URL}/login",
        data=data,
    )
    if r.status_code != 200:
        if "no-2fa-code" in r.text:
            mfa_code = getpass.getpass("2FA code: ")
            return roadai_login(args, password, mfa_code)
        raise Exception(f'Got {r.status_code}: "{r.text}" from /login')

    json_resp = r.json()
    user_id = json_resp["data"]["userId"]
    auth_token = json_resp["data"]["authToken"]
    print(f"Logged in as {args.username}")

    return (user_id, auth_token)


def roadai_logout(user_id: str, auth_token: str):
    r = requests.post(
        f"{API_BASE_URL}/logout",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
    )
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code} from /logout")
    print("Logged out successfully")


def fetch_point_objects(user_id: str, auth_token: str, args: argparse.Namespace):
    r = requests.get(
        f"{API_BASE_URL}/{args.point_object_endpoint}",
        headers={
            "X-User-Id": user_id,
            "X-Auth-Token": auth_token,
        },
        params={
            "client_id": args.client_id,
            "date_from_recorded": args.recorded_begin,
            "date_to_recorded": args.recorded_end,
            "tags_in": args.tags,
            # 100 items is the maximum number of records that can be fetched with a
            # single request. If you want to fetch the next 100 records, use the
            # "cursornext" link that is returned in the "Links" response header.
            "limit": 100,
        },
    )
    if r.status_code != 200:
        raise Exception(f"Got {r.status_code} from /{args.point_object_endpoint}")
    return r.json()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--username", type=str, required=True, help="RoadAI username")
    # Example: abcdefgh12345678
    parser.add_argument("--client_id", type=str, required=True, help="RoadAI client ID")
    # Example: 2023-01-20T00:00:00.000Z
    parser.add_argument(
        "--recorded_begin",
        type=str,
        required=True,
        help="Include data recorded after this timestamp (ISO 8601)",
    )
    # Example: 2023-02-20T00:00:00.000Z
    parser.add_argument(
        "--recorded_end",
        type=str,
        required=True,
        help="Include data recorded before this timestamp (ISO 8601)",
    )
    parser.add_argument(
        "--point_object_endpoint",
        type=str,
        required=True,
        choices=["potholes", "surfaceMarkings", "trafficSigns"],
        help="Point object endpoint (e.g. potholes)",
    )
    parser.add_argument(
        "--tags",
        type=str,
        default="",
        help="Comma-separated list of tags to filter point objects",
    )
    args = parser.parse_args()

    user_id, auth_token = roadai_login(args)

    point_objects = fetch_point_objects(user_id, auth_token, args)

    print(f"Found {len(point_objects)} point objects")
    print(f"First point object: {point_objects[0]}")

    roadai_logout(user_id, auth_token)
