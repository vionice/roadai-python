""" Upload file examples """
import os
import io
import time
import glob
import logging
import unittest
import tempfile
import zipfile
import getpass
from typing import Dict, IO, List, Optional, Any, Callable
from roadai.api import Api
from tusclient import client
from tusclient.exceptions import TusCommunicationError


def timeit(method: Callable) -> Callable:
    """ Timeit decorator @timeit """

    def timed(*args: Any, **kw: Any) -> Any:
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            logging.info('%r  %2.2f ms', method.__name__, (te - ts) * 1000)
        return result

    return timed


@timeit
def send_file(auth_data: Dict, share: Dict, filehandle: IO, filename: str) -> bool:
    """ Upload a file using tusd client """

    url = os.getenv('TUSD_URL', 'https://tusd.vionice.io/files/')

    metadata = {
        "userId": auth_data['userId'],
        "loginToken": auth_data['authToken'],
        "filename": filename,
        "share": share['id']
    }

    tus_client = client.TusClient(url)
    uploader = tus_client.uploader(file_stream=filehandle, chunk_size=32768, metadata=metadata)

    try:
        uploader.upload()
    except TusCommunicationError as err:
        logging.error("Upload failed %s", str(err))
        return False
    else:
        return True


def get_share(api: Api, share_name: Optional[str]) -> Dict:
    """ Helper to get share """

    share = None
    shares = api.shares()
    if not share_name and shares:
        share = shares[0]
        logging.warning("No specific share, selecting first %s", str(share))
    elif shares:
        share = next((i for i in shares if i['name'] == share_name), None)

    if share is None:
        logging.error("No share %s defined", share_name)
        return {}
    else:
        return share


def upload_video(
    username: str,
    password: str,
    filename_vid: str,
    filename_loc: str,
    share_name: Optional[str] = None
) -> bool:
    """ Upload zipped videos """

    api = Api()
    auth_data = api.login(username, password)
    share = get_share(api, share_name)

    filename = "video_upload.zip"
    with tempfile.TemporaryFile() as tmp:
        with zipfile.ZipFile(tmp, 'w', zipfile.ZIP_DEFLATED) as archive:
            archive.write(filename_vid)
            archive.write(filename_loc)

        logging.info("Uploading %s, size %s", filename, os.path.getsize(tmp.name) / 1e6)
        success = send_file(auth_data, share, tmp, filename)

    api.logout()
    return success


class FailedToAuthenticateException(Exception):
    pass


class NoShareException(Exception):
    pass


def upload_MD30_zip(
    username: str,
    password: str,
    filenames: List,
    share_name: Optional[str] = None
) -> bool:

    api = Api()
    auth_data = api.login(username, password)
    if not auth_data:
        raise FailedToAuthenticateException("Auth failed")

    share = get_share(api=api, share_name=share_name)
    if not share:
        raise NoShareException("Share not found")

    with tempfile.TemporaryFile() as upload_archive:
        with zipfile.ZipFile(upload_archive, 'w', zipfile.ZIP_DEFLATED) as destination_archive:
            for filename in filenames:
                destination_archive.write(filename=filename)
        destination_filename = "md30_data.zip"
        logging.info("Uploading %s, size %s MB", destination_filename, os.path.getsize(upload_archive.name) / 1e6)
        success = send_file(auth_data=auth_data, share=share, filehandle=upload_archive, filename=destination_filename)

    api.logout()
    return success


def upload_image_zip(
    username: str,
    password: str,
    filenames: List,
    share_name: Optional[str] = None
) -> bool:
    """ Upload zipped images """

    api = Api()
    auth_data = api.login(username, password)
    share = get_share(api, share_name)
    if not share:
        return False

    destination_filename = "images.zip"
    if not auth_data:
        return False

    with tempfile.TemporaryFile() as tmp:
        with zipfile.ZipFile(tmp, 'w', zipfile.ZIP_DEFLATED) as archive:
            for filename in filenames:
                archive.write(filename)

        logging.info("Uploading %s, size %s", filename, os.path.getsize(tmp.name) / 1e6)
        success = send_file(auth_data, share, tmp, destination_filename)

    api.logout()
    return success


def upload_image(
    username: str,
    password: str,
    filename: str,
    share_name: Optional[str] = None
) -> bool:
    """ Upload a single image """

    api = Api()
    auth_data = api.login(username, password)
    share = get_share(api, share_name)

    with open(filename, 'rb') as fin:
        data = io.BytesIO(fin.read())
        success = send_file(auth_data, share, data, filename)

    data.close()
    return success


class TestUpload(unittest.TestCase):
    """ Test upload methods """

    username = input("API username: ")
    password = getpass.getpass("API password: ")

    def test_upload_video_zip(self) -> None:
        """ Test uploading videos inside a zip file """
        filename_vid = "/test_data/test_segment.mp4"
        filename_loc = "/test_data/test_segment.gpx"
        output = upload_video(self.username, self.password, filename_vid, filename_loc)
        self.assertTrue(output)

    def test_upload_image_zip(self) -> None:
        """ Test uploading images inside a zip file """
        filenames = glob.glob("/test_data/*.jpeg")
        output = upload_image_zip(self.username, self.password, filenames)
        self.assertTrue(output)

    def test_upload_image(self) -> None:
        """ Test uploading bare image """
        filename = "/test_data/2018-11-01T15_20_26.588000.jpeg"
        output = upload_image(self.username, self.password, filename)
        self.assertTrue(output)

    def test_upload_md30_zip(self) -> None:
        """ Test uploading MD30 data """
        filenames = [
            "/test_data/test_md30_serial.bin",
            "/test_data/test_md30_location.gpx",
        ]
        output = upload_MD30_zip(self.username, self.password, filenames)
        self.assertTrue(output)


if __name__ == "__main__":
    FR_B = "%(asctime)s,%(msecs)d %(levelname)-8s"
    FR_E = ' [%(filename)s:%(lineno)d] %(message)s'
    DATE_FRM = '%d-%m-%Y:%H:%M:%S'
    logging.basicConfig(format=FR_B + FR_E, datefmt=DATE_FRM, level=logging.DEBUG)
    unittest.main()
