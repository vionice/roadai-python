# A script to download MD30 data from RoadAI API and group the full frequency output into CSV files by recording sessions.
#
# INPUT
# - username
# - password
#
# OUTPUT
#  CSV files for each recording session in the following file structure:
#  '{client_name}-{client_id}/{share_name}-{share_id}/{username}/{session_tag}-session-full-frequency.csv'
#
# For example
#
# Vaisala MD30 - <unique client id>/
#     Default share - <unique share id>/
#         Vaisala_0001/
#             2019-11-12_14-22-58-904-0200-session-full-frequency.csv
#             2019-11-13_12-06-05-420-0200-session-full-frequency.csv
#             ...
#         Vaisala_0002/
#             2019-10-12_14-22-58-904-0200-session-full-frequency.csv
#             2019-10-13_12-06-05-420-0200-session-full-frequency.csv
#             ...



# Note: required only for local import
import os,sys
import pandas as pd
import gpxpy
import getpass
import logging
from datetime import datetime
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from io import BytesIO

from roadai.api import Api
from roadai.md30 import MD30Parser, RecordParser, SendData, legacy

FORMAT = '%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s'
logging.basicConfig(format=FORMAT,
                    datefmt='%d-%m-%Y:%H:%M:%S',
                    level=logging.INFO)

api = Api()
api.login(
    input("user: "),
    getpass.getpass("pass: ")
)


# Surface State - MD30 Sensor
PARAMETER_TYPES = [
    "SURFACE_STATE",
    "SURFACE_GRIP",
    "AIR_TEMPERATURE",
    "SURFACE_TEMPERATURE",
    "RELATIVE_HUMIDITY",
    "DEW_POINT_TEMPERATURE",
    "WATER_LAYER_THICKNESS",
    "SNOW_LAYER_THICKNESS",
    "ICE_LAYER_THICKNESS"
]


shares = api.shares()

for share in shares:
    # Limit query to certain shares
    # if share["id"] not in ["P7eacjxsHZHRyEGZw"]:
    #     continue

    query = {
        "share_id": share["id"],
        "client_id": share["client_id"],
        # "date_from_recorded": datetime(2019, 3, 1, 0, 0, 1),
        # "date_to_recorded": datetime(2019, 3, 15, 23, 59, 59),
        "sort": "recorded_at",
        "parameter_type": ",".join(PARAMETER_TYPES)  # Fetch mobileObservations that include defined parameters
    }

    observations = api.mobileObservations(**query)

    if len(observations) == 0:
        logging.error("no md30 data in this share")
        exit()

    # Observations are sorted in descending recroded_at order (API don't support sort direction atm).
    # => Reverse the list so that rows in CSV file are in ascending order
    for obs in observations[::-1]:

        serial_data = api.download_file(obs, 'serial.bin')
        if serial_data is None:
            logging.info("trying legacy fileformat")
            legacy_data = api.download_file(obs, 'serial.csv')
            if legacy_data is None:
                logging.error("no md30 files found")
                continue
            serial_reader = legacy.Convert(BytesIO(legacy_data))
        else:
            serial_reader = BytesIO(serial_data)

        location_data = api.download_file(obs, 'location.gpx')

        rows = []
        for index, record in enumerate(RecordParser(serial_reader, parser=MD30Parser())):
            if isinstance(record, RecordParser.DataRecord) and \
                    isinstance(record.data, SendData):
                rows.append(record)

        gpx = gpxpy.parse(location_data.decode())
        gps_points = gpx.tracks[0].segments[0].points

        sensor = pd.DataFrame(index=[r.time for r in rows], data=[dict(r.data) for r in rows], dtype='object')
        gps = pd.DataFrame(index=[p.time.replace(tzinfo=None) for p in gps_points], data=[{'longitude':p.longitude, 'latitude': p.latitude} for p in gps_points])

        # interpolate gps points for measurements
        sensor_gps = gps.join(sensor, how='outer')
        if 'latitude' in sensor_gps:
            sensor_gps.latitude.interpolate(method='time', limit_direction='both', inplace=True)
        if 'longitude' in sensor_gps:
            sensor_gps.longitude.interpolate(method='time', limit_direction='both', inplace=True)

        # Remove rows with missing MD30 data. Missing GPS coordinates are allowed.
        sensor_gps.dropna(subset=sensor_gps.columns.difference(['longitude', 'latitude']), how='all', inplace=True)

        # Observations are fixed length batches (e.g. 5min) but recording session starts and ends when the recording button is pressed.
        # Will combine all observations from the same recording session
        filename = '{client_name}-{client_id}/{share_name}-{share_id}/{username}/{session_tag}-session-full-frequency.csv'.format(
            client_name=share['client_name'],
            client_id=share['client_id'],
            share_name=share['name'].replace('/', '_'),
            share_id=share['id'],
            username=obs['user'],
            session_tag=obs['session_tag'])

        # If the file name exists, just append measurements without header row.
        if os.path.exists(filename):
            with open(filename, 'a') as f:
                sensor_gps.to_csv(f, date_format='%Y-%m-%dT%H:%M:%S.%fZ', index_label='timestamp', header=False)
            logging.info("{} appended".format(filename))

        # Otherwise include the header row as well
        else:
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            sensor_gps.to_csv(filename, date_format='%Y-%m-%dT%H:%M:%S.%fZ', index_label='timestamp', header=True)
            logging.info("{} created".format(filename))

api.logout()
