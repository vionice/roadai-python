# Note: required only for local import
import os,sys
import pandas as pd
import gpxpy
sys.path.insert(0, os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from io import BytesIO

from collections import defaultdict
from pprint import pprint
from csv import DictWriter
from roadai.api import Api
from roadai.md30 import MD30Parser, RecordParser, SendData, legacy

api = Api()
api.login(
    input("user: "),
    input("pass: ")
)


shares = api.shares(limit=100)
for i, share in enumerate(shares):
    print(i, share['name'])

share = shares[int(input("share index: "))]
print("share:", share)

# Surface State - MD30 Sensor
PARAMETER_TYPES = [
    "SURFACE_STATE",
    "SURFACE_GRIP",
    "AIR_TEMPERATURE",
    "SURFACE_TEMPERATURE",
    "RELATIVE_HUMIDITY",
    "DEW_POINT_TEMPERATURE",
    "WATER_LAYER_THICKNESS",
    "SNOW_LAYER_THICKNESS",
    "ICE_LAYER_THICKNESS"
]

# Road Weather - Computer Vision
# PARAMETER_TYPES = [
#     "SURFACE_STATE_CV",
#     "SURFACE_SNOW_CV",
#     "SURFACE_WATER_CV"
#     ...
# ]

# Pavement Condition - Computer Vision
# PARAMETER_TYPES = [
#     "PAVEMENT_CONDITION_CV",
#     "SEVERE_POTHOLE_CV",
#     "SEVERE_LONGITUDINAL_CRACKING_CV"
#     ...
# ]

# Assets - Computer Vision
# PARAMETER_TYPES = [
#     "GUARD_RAIL_RIGHT_CV",
#     "GUARD_RAIL_LEFT_CV",
#     "TRAFFIC_CONE_RIGHT_CV",
#     "TRAFFIC_CONE_LEFT_CV"
#     ...
# ]


query = {
    "share_id": share['id'],                     # Fetch documents from the first available share
    "limit": 1,                                  # Limit search to 1 document
    "parameter_type": ",".join(PARAMETER_TYPES)  # Fetch mobileObservations that include defined parameters
}

observations = api.mobileObservations(**query)

for obs in observations:
    # Print summary of GPS points ("locations.coordinates") and timestamps ("location_times") that are
    # located on the root of the mobileObservations document
    print("\n{:<16}\t{:<16}".format(
        "document key",
        "# of GPS points"))

    print("{:<16}\t{:<16}".format(
        "locations.coordinates",
        len(obs["locations"]["coordinates"])))

    print("{:<16}\t{:<16}".format(
        "location_times",
        len(obs["location_times"])))


    # Print summary of measurements associated with the locations. Parameter values are located in "location_measurements" array.
    print("\n{:<16}\t{:<16}\t{:<16}\t{:<16}".format(
        "parameter_type",
        "# of measurements",
        "unit",
        "device_type"))
    for m in obs["location_measurements"]:

        if m["parameter_type"] in PARAMETER_TYPES:
            print("{:<16}\t{:<16}\t{:<16}\t{:<16}".format(
                m["parameter_type"],
                len(m["measurements"]),
                m["unit"],
                m["device_type"]))

if len(observations) == 0:
    print("no md30 data in this share")
    exit()


serial_data = api.download_file(observations[0], 'serial.bin')
if serial_data is None:
    print("trying legacy fileformat")
    legacy_data = api.download_file(observations[0], 'serial.csv')
    if legacy_data is None:
        print("no md30 files found")
        exit()
    serial_reader = legacy.Convert(BytesIO(legacy_data))
else:
    serial_reader = BytesIO(serial_data)

location_data = api.download_file(observations[0], 'location.gpx')

rows = []
for index, record in enumerate(RecordParser(serial_reader, parser=MD30Parser())):
    if isinstance(record, RecordParser.DataRecord) and \
            isinstance(record.data, SendData):
        rows.append(record)

gpx = gpxpy.parse(location_data.decode())
gps_points = gpx.tracks[0].segments[0].points

sensor = pd.DataFrame(index=[r.time for r in rows], data=[dict(r.data) for r in rows], dtype='object')
gps = pd.DataFrame(index=[p.time.replace(tzinfo=None) for p in gps_points], data=[{'longitude':p.longitude, 'latitude': p.latitude} for p in gps_points])

# interpolate gps points for measurements
sensor_gps = gps.join(sensor, how='outer')
sensor_gps.latitude.interpolate(method='time', limit_direction='both', inplace=True)
sensor_gps.longitude.interpolate(method='time', limit_direction='both', inplace=True)
sensor_gps.dropna(inplace=True)
sensor_gps.to_csv('measurements-full-frequency.csv', date_format='%Y-%m-%dT%H:%M:%S.%fZ', index_label='timestamp')
print("measurements-full-frequency.csv written")

# nearest measurements for gps points
gps_sensor = gps.join(sensor, how='outer')
for col in gps_sensor.columns:
    if col in ('latitude', 'longitude'):
        continue
    gps_sensor[col].ffill(inplace=True, downcast={})
    gps_sensor[col].bfill(inplace=True, downcast={})
gps_sensor.dropna(inplace=True)
gps_sensor.to_csv('measurements-database.csv', date_format='%Y-%m-%dT%H:%M:%S.%fZ', index_label='timestamp')
print("measurements-database.csv written")


api.logout()
