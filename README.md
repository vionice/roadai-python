## roadai-python

[![pipeline status](https://gitlab.com/vionice/roadai-python/badges/master/pipeline.svg)](https://gitlab.com/vionice/roadai-python/commits/master)
[![coverage report](https://gitlab.com/vionice/roadai-python/badges/master/coverage.svg)](https://gitlab.com/vionice/roadai-python/commits/master)

A python library to access Vaisala RoadAI API: https://swagger.vionice.io  
Documentation: https://vionice.gitlab.io/roadai-python/


## Installation
```
sudo pip install --upgrade git+https://gitlab.com/vionice/roadai-python
```

## Code examples

    
A) Download and parse full MD30 data: See `examples/download-MD30-full-frequency.py`   
    

B) Load parameter values in the same resolution (at most 10 meters between two consecutive measurement) as displayed in https://map.vionice.io (See `examples/mobileObservations-parameters.py`)   
   
## Running examples

One way to test examples quickly is run followind docker commands

```
docker build -t roadai-python .
docker run -ti \
    -v $(pwd)/roadai:/examples/roadai/ \
    -v $(pwd)/examples:/examples \
    -v $(pwd)/test_data:/test_data \
    -w /examples \
    roadai-python /bin/bash
```


### Prerequisite: Login and fetch mobile obs
Import and login
```python
from roadai.api import Api

api = Api()
api.login('john.doe@anonymous.com', 'myverysecretpassword')
```

List shares you have access to
```python
shares = api.shares()

print(len(shares))
for share in shares:
	print(share)
```

```console
# console output ...

shares:  1
{'id': 'zAPAwwhwdwCEJXBNz', 'name': 'Vaisala_test/default', 'client_id': '132c7b2391a4512c69eda509', 'client_name': 'Vaisala_test', 'permissions': ['read', 'write', 'edit', 'admin']}
```

Download the most recent video from a given share that include defined parameters
```python
# Surface State - MD30 Sensor
PARAMETER_TYPES = [
    "SURFACE_STATE",
    "SURFACE_GRIP",
    "AIR_TEMPERATURE",
    "SURFACE_TEMPERATURE",
    "RELATIVE_HUMIDITY",
    "DEW_POINT_TEMPERATURE",
    "WATER_LAYER_THICKNESS",
    "SNOW_LAYER_THICKNESS",
    "ICE_LAYER_THICKNESS"
]

# Road Weather - Computer Vision
# PARAMETER_TYPES = [
#     "SURFACE_STATE_CV",
#     "SURFACE_SNOW_CV",
#     "SURFACE_WATER_CV"
#     ...
# ]

# Pavement Condition - Computer Vision
# PARAMETER_TYPES = [
#     "PAVEMENT_CONDITION_CV",
#     "SEVERE_POTHOLE_CV",
#     "SEVERE_LONGITUDINAL_CRACKING_CV"
#     ...
# ]

# Assets - Computer Vision
# PARAMETER_TYPES = [
#     "GUARD_RAIL_RIGHT_CV",
#     "GUARD_RAIL_LEFT_CV",
#     "TRAFFIC_CONE_RIGHT_CV",
#     "TRAFFIC_CONE_LEFT_CV"
#     ...
# ]

query = {
    "share_id": shares[0]["id"],
    "limit": 1,
    "parameter_type": ",".join(PARAMETER_TYPES),
}

observations = api.mobileObservations(**query)
```

### Load parameter values at the resolution seen in https://map.vionice.io    
```python
for obs in observations:
    # Print summary of GPS points ("locations.coordinates") and timestamps ("location_times") that are
    # located on the root of the mobileObservations document
    print("\n{:<16}\t{:<16}".format(
        "document key",
        "# of GPS points"))

    print("{:<16}\t{:<16}".format(
        "locations.coordinates",
        len(obs["locations"]["coordinates"])))

    print("{:<16}\t{:<16}".format(
        "location_times",
        len(obs["location_times"])))


    # Print summary of measurements associated with the locations. Parameter values are located in
    # "location_measurements" array.
    print("\n{:<16}\t{:<16}\t{:<16}\t{:<16}".format(
        "parameter_type",
        "# of measurements",
        "unit",
        "device_type"))
    for m in obs["location_measurements"]:

        if m["parameter_type"] in PARAMETER_TYPES:
            print("{:<16}\t{:<16}\t{:<16}\t{:<16}".format(
                m["parameter_type"],
                len(m["measurements"]),
                m["unit"],
                m["device_type"]))
```

```console
# console output ...

document key            # of GPS points
locations.coordinates   174
location_times          174

parameter_type          # of measurements       unit                    device_type
SURFACE_GRIP            174                     COEFFICIENT             MD30
WATER_LAYER_THICKNESS   174                     MILLIMETRES             MD30
ICE_LAYER_THICKNESS     174                     MILLIMETRES             MD30
SNOW_LAYER_THICKNESS    174                     MILLIMETRES             MD30
SURFACE_STATE           174                     SURFACE_STATE           MD30
SURFACE_TEMPERATURE     174                     DEGREES_CELSIUS         MD30
AIR_TEMPERATURE         174                     DEGREES_CELSIUS         MD30
RELATIVE_HUMIDITY       174                     PERCENT                 MD30
DEW_POINT_TEMPERATURE   174                     DEGREES_CELSIUS         MD30
```


By logging out, you explicitly invalidate the login token that was created for you
```python
api.logout()
```
