#!/bin/bash

set -e
set -x

# Linter
mypy roadai --ignore-missing-imports

# Test coverage
coverage run --source roadai -m unittest discover -v
coverage report -m

echo "Tests passed!"
